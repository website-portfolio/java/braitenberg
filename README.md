# Braitenberg vehicles

## Our project

This project was made together with T. Joosten and C. Verploegen, for the course Visual Computing Project . The goal was to explain and show how Braitenberg vehicles work and how they interact with a world, and this is the end result.

<div align="center">

![Braitenberg Vehicle Final showing](./assets/Images/Home.png)

</div>



## Braitenberg Vehicles

Wikipedia:

*A Braitenberg vehicle is a concept conceived in a thought experiment by the Italian-Austrian cyberneticist Valentino Braitenberg. The book models the animal world in a minimalistic and constructive way, from simple reactive behaviours (like phototaxis) through the simplest vehicles, to the formation of concepts, spatial behaviour, and generation of ideas.*

As an example take the following interaction where the wheels of a vehicle will go faster (**+**) or slower (**-**), depending on the amount of lights the front sensors (arms) see.


<div align="center">

![Braitenberg vehicles behaviour](./images/braitenberg.png)

</div>




## Installation

1) This project requires the [`JMonkeyEngine`](https://jmonkeyengine.org/) (JME)
2) Download these folders
3) Import the project folder using JME, and you should be ready to go.



