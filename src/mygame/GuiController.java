package mygame;

// Nifty GUI
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * GuiController class
 */
public class GuiController implements ScreenController {
    
//    Variable declarations
    final private Main mainClass;
    

    GuiController(Main main) {
        mainClass = main;
    }
    
    
//    Base methods
    @Override
    public void bind(Nifty nifty, Screen screen) {
        System.out.println("bind(" + screen.getScreenId() + ")");
    }

    @Override
    public void onStartScreen() {
        System.out.println("onStartScreen");
    }

    @Override
    public void onEndScreen() {
        System.out.println("onEndScreen");
    }
    
    
//    Custom Gui event methods
    public void startButtonClicked() {
        System.out.println("Start button clicked");
        // Call startRun method in Main class
        mainClass.startRun();
    }
    
    public void pauseButtonClicked() {
        System.out.println("Pause button clicked");
        mainClass.pauseRun();
    }
    
    public void stopButtonClicked() {
        System.out.println("Stop button clicked");
        mainClass.stopRun();
    }
    
    public void light1ButtonClicked() {
        System.out.println("Light 1 button clicked");
        mainClass.placeNinja();
        // TODO place light 1
    }
    
    public void light2ButtonClicked() {
        System.out.println("Light 2 button clicked");
        // TODO place light 2
    }
    
    public void vehicle1ButtonClicked() {
        System.out.println("Vehicle 1 button clicked");
        // TODO place vehicle 1
    }
    
    public void vehicle2ButtonClicked() {
        System.out.println("Vehicle 2 button clicked");
        // TODO place vehicle 2
    }
    
    public void importSceneButtonClicked() {
        System.out.println("Import scene button clicked");
        mainClass.openSceneScreen();
    }
    
    public void closeSceneScreenButtonClicked() {
        System.out.println("Close scene screen button clicked");
        mainClass.closeSceneScreen();
    }
    
    public void scene1ButtonClicked() {
        System.out.println("Scene 1 button clicked");
        mainClass.importScene(1);
    }
    
    public void scene2ButtonClicked() {
        System.out.println("Scene 2 button clicked");
        mainClass.importScene(2);
    }
    
    public void scene3ButtonClicked() {
        System.out.println("Scene 3 button clicked");
        mainClass.importScene(3);
    }
    
    public void scene4ButtonClicked() {
        System.out.println("Scene 4 button clicked");
        mainClass.importScene(4);
    }
    
    /**
     * Build gui screen
     * @param nifty
     * @param gc
     */
    public void buildGuiScreen(final Nifty nifty, final GuiController gc) {
        nifty.addScreen("gui", new ScreenBuilder("gui") {{
            controller(gc);

            layer(new LayerBuilder("foreground") {{
                childLayoutHorizontal();
                backgroundColor("#0000");

                panel(new PanelBuilder("panel_left") {{
                    childLayoutVertical();
                    height("100%");
                    width("80%");
                }});

                panel(new PanelBuilder("panel_right") {{
                    childLayoutVertical();
                    backgroundColor("#222222f3");
                    height("100%");
                    width("20%");

                    // Title panel
                    panel(new PanelBuilder("title") {{
                        childLayoutCenter();
                        height("10%");
                        width("100%");

                        control(new LabelBuilder(){{
                            color("#fff");
                            text("Braitenberg");
                            width("100%");
                            height("100%");
                        }});
                    }});

                    // Icons panel
                    panel(new PanelBuilder("icons") {{
                        childLayoutHorizontal();
                        backgroundColor("#fff5");
                        height("10%");
                        width("100%");
                        visibleToMouse(true);
                        
                        // Icons made by Pixel perfect from www.flaticon.com
                        // Stop button
                        panel(new PanelBuilder("icons_stop") {{
                            childLayoutCenter();
                            height("100%");
                            width("33%");
                        
                            image(new ImageBuilder() {{
                                valignCenter();
                                height("30px");
                                width("30px");
                                filename("Images/stop.png");
                                interactOnClick("stopButtonClicked()");
                            }});
                        }});
                        
                        // Pause button
                        panel(new PanelBuilder("icons_pause") {{
                            childLayoutCenter();
                            height("100%");
                            width("33%");
                            
                            image(new ImageBuilder() {{
                                valignCenter();
                                height("30px");
                                width("30px");
                                filename("Images/pause.png");
                                interactOnClick("pauseButtonClicked()");
                            }});
                        }});
                        
                        // Start button
                        panel(new PanelBuilder("icons_start") {{
                            childLayoutCenter();
                            height("100%");
                            width("33%");
                            
                            image(new ImageBuilder() {{
                                valignCenter();
                                height("30px");
                                width("30px");
                                filename("Images/play.png");
                                interactOnClick("startButtonClicked()");
                            }});
                        }});
                    }});

                    // Lights panel
                    panel(new PanelBuilder("lights") {{
                        childLayoutVertical();
                        height("22%");
                        width("100%");
                        
                        panel(new PanelBuilder("lights_text") {{
                            childLayoutCenter();
                            backgroundColor("#111d");
                            height("25%");
                            width("100%");
                            
                            control(new LabelBuilder(){{
                                color("#fff");
                                text("Lights");
                                width("100%");
                                height("100%");
                            }});
                        }});
                        
                        panel(new PanelBuilder("lights_select") {{
                            childLayoutHorizontal();
                            height("75%");
                            width("100%");
                            
                            panel(new PanelBuilder("lights_select1") {{
                                childLayoutCenter();
                                height("100%");
                                width("50%");

                                image(new ImageBuilder() {{
                                    valignCenter();
                                    height("50px");
                                    width("50px");
                                    filename("Images/play.png");
                                    interactOnClick("light1ButtonClicked()");
                                }});
                            }});
                            
                            panel(new PanelBuilder("lights_select2") {{
                                childLayoutCenter();
                                height("100%");
                                width("50%");

                                image(new ImageBuilder() {{
                                    valignCenter();
                                    height("50px");
                                    width("50px");
                                    filename("Images/play.png");
                                    interactOnClick("light2ButtonClicked()");
                                }});
                            }});
                        }});
                    }});
                    
                    // Vehicles panel
                    panel(new PanelBuilder("vehicles") {{
                        childLayoutVertical();
                        height("22%");
                        width("100%");
                        
                        panel(new PanelBuilder("vehicles_text") {{
                            childLayoutCenter();
                            backgroundColor("#111d");
                            height("25%");
                            width("100%");
                            
                            control(new LabelBuilder(){{
                                color("#fff");
                                text("Vehicles");
                                width("100%");
                                height("100%");
                            }});
                        }});
                        
                        panel(new PanelBuilder("vehicles_select") {{
                            childLayoutHorizontal();
                            height("75%");
                            width("100%");
                            
                            panel(new PanelBuilder("vehicles_select1") {{
                                childLayoutCenter();
                                height("100%");
                                width("50%");

                                image(new ImageBuilder() {{
                                    valignCenter();
                                    height("50px");
                                    width("50px");
                                    filename("Images/vehicle1.png");
                                    interactOnClick("vehicle1ButtonClicked()");
                                }});
                            }});
                            
                            panel(new PanelBuilder("vehicles_select2") {{
                                childLayoutCenter();
                                height("100%");
                                width("50%");

                                image(new ImageBuilder() {{
                                    valignCenter();
                                    height("50px");
                                    width("50px");
                                    filename("Images/vehicle2.png");
                                    interactOnClick("vehicle2ButtonClicked()");
                                }});
                            }});
                        }});
                    }});
                    
                    // Stats panel
                    panel(new PanelBuilder("stats") {{
                        childLayoutVertical();
                        height("26%");
                        width("100%");
                        
                        panel(new PanelBuilder("stats_text") {{
                            childLayoutCenter();
                            backgroundColor("#111d");
                            height("25%");
                            width("100%");
                            
                            control(new LabelBuilder(){{
                                color("#fff");
                                text("Stats");
                                width("100%");
                                height("100%");
                            }});
                        }});
                        
                        panel(new PanelBuilder("stats_select") {{
                            childLayoutHorizontal();
                            height("75%");
                            width("100%");
                            
                            control(new LabelBuilder(){{
                                color("#fff");
                                text("Import");
                                width("100%");
                                height("100%");
                                interactOnClick("importSceneButtonClicked()");
                            }});
                        }});
                    }});
                }});
            }});
        }}.build(nifty));
    }
    
    /**
     * Build scene screen
     * @param nifty
     * @param gc
     */
    public void buildSceneScreen(final Nifty nifty, final GuiController gc) {
        nifty.addScreen("scene", new ScreenBuilder("scene") {{
            controller(gc);

            layer(new LayerBuilder("foreground2") {{
                childLayoutHorizontal();
                backgroundColor("#222222f3");

                // Scenes panel
                panel(new PanelBuilder("scenes") {{
                    childLayoutVertical();
                    height("100%");
                    width("100%");

                    panel(new PanelBuilder("scenes_text") {{
                        childLayoutHorizontal();
                        backgroundColor("#111d");
                        height("10%");
                        width("100%");

                        control(new LabelBuilder(){{
                            color("#fff");
                            text("Scenes");
                            width("85%");
                            height("100%");
                        }});

                        panel(new PanelBuilder("scenes_close") {{
                            childLayoutCenter();
                            height("100%");
                            width("15%");

                            image(new ImageBuilder() {{
                                valignCenter();
                                height("30px");
                                width("30px");
                                filename("Images/close.png");
                                interactOnClick("closeSceneScreenButtonClicked()");
                            }});
                        }});
                    }});

                    panel(new PanelBuilder("scenes_select_a") {{
                        childLayoutHorizontal();
                        height("45%");
                        width("100%");

                        panel(new PanelBuilder("scenes_select1") {{
                            childLayoutCenter();
                            height("100%");
                            width("50%");

                            image(new ImageBuilder() {{
                                valignCenter();
                                height("50px");
                                width("50px");
                                filename("Images/play.png");
                                interactOnClick("scene1ButtonClicked()");
                            }});
                        }});

                        panel(new PanelBuilder("scenes_select2") {{
                            childLayoutCenter();
                            height("100%");
                            width("50%");

                            image(new ImageBuilder() {{
                                valignCenter();
                                height("50px");
                                width("50px");
                                filename("Images/play.png");
                                interactOnClick("scene2ButtonClicked()");
                            }});
                        }});
                    }});

                    panel(new PanelBuilder("scenes_select_b") {{
                        childLayoutHorizontal();
                        height("45%");
                        width("100%");

                        panel(new PanelBuilder("scenes_select3") {{
                            childLayoutCenter();
                            height("100%");
                            width("50%");

                            image(new ImageBuilder() {{
                                valignCenter();
                                height("50px");
                                width("50px");
                                filename("Images/play.png");
                                interactOnClick("scene3ButtonClicked()");
                            }});
                        }});

                        panel(new PanelBuilder("scenes_select4") {{
                            childLayoutCenter();
                            height("100%");
                            width("50%");

                            image(new ImageBuilder() {{
                                valignCenter();
                                height("50px");
                                width("50px");
                                filename("Images/play.png");
                                interactOnClick("scene4ButtonClicked()");
                            }});
                        }});
                    }});
                }});
            }});
        }}.build(nifty));
    }
    
}