/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.objects;

// Root packages
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;

// Collision things
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.FastMath;

// Object placement and rotation
import com.jme3.scene.Spatial;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector2f;
import com.jme3.math.Quaternion;
import java.util.List;

/**
 *
 *
 */
public class Robot {

    // From main for placing objects and getting assets
    private final Node rootNode;
    private final AssetManager assetManager;
    private final PhysicsSpace physicsSpace;

    // Robot definitions
    private final Node robotNode = new Node("robotNode");
    private CharacterControl robotControl;
    private Spatial robotSpatial;
    boolean robotType;  // Normal if true, Crossed if false
    boolean robotPlusMotor; // Faster if True, slower if false

    Vector3f startPosition;
    Vector3f startDirection;
    float startSpeed = 0.008f;
    float maxSpeed = 0.01f;
    float minSpeed = 0f;
    String robotSettings;
    String modelPath;

    // Define robot interaction
    Vector3f movement;
    float speed = startSpeed;

    // Define robot states
    boolean robotFreeze = false;
    public boolean selected = false;

    // Selection variables
    Spatial selectArrow;

    /**
     * Constructor of the robot
     *
     * @param rootNode
     * @param assetManager
     * @param physicsSpace
     */
    public Robot(Node rootNode, AssetManager assetManager, PhysicsSpace physicsSpace) {
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.physicsSpace = physicsSpace;
    }

    /**
     * Creates the robot spatial
     *
     * @param startPos
     * @param dir
     * @param robotSettings
     * @param modelPath
     */
    public void create(Vector3f startPos, Vector3f dir, String robotSettings, String modelPath) {

        // Store variables for resetting.
        startPosition = new Vector3f(startPos.x, 1.5f, startPos.z);
        startDirection = dir;
        this.robotSettings = robotSettings;
        this.modelPath = modelPath;

        this.robotType = robotSettings.toLowerCase().startsWith("normal");
        this.robotPlusMotor = robotSettings.toLowerCase().endsWith("fast");

        // Create robot set translation and rotation
        robotSpatial = assetManager.loadModel(modelPath);
        robotSpatial.setLocalTranslation(startPosition);
        rootNode.attachChild(robotSpatial);

        // Create the collision shapes
//        this.initCollisions();

        // Define directions
        if (dir.x == 0f && dir.y == 0f && dir.z == 0f) {
            dir = new Vector3f(0, 0, speed);
        }

        // And movement
        movement = dir.normalize().mult(speed);

        createSelectArrow();
        updateAngle();
    }

    /**
     * Initialize selection arrow
     */
    public void createSelectArrow() {
        selectArrow = assetManager.loadModel("Models/Arrow/Arrow.j3o");
        Quaternion x90Rot = new Quaternion();
        x90Rot.fromAngleAxis((float) -Math.PI / 2, new Vector3f(1, 0, 0));
        selectArrow.setLocalRotation(x90Rot);
        selectArrow.scale(0.02f);
    }

    /**
     * Initialize collision shapes and control. Not working
     */
    @Deprecated
    public void initCollisions() {
        
        BoxCollisionShape capsuleShape = new BoxCollisionShape(new Vector3f(0.8f, 0.8f, 1.5f));
        robotControl = new CharacterControl(capsuleShape, 400f);
        
        robotControl.setUseViewDirection(true);
        robotControl.setPhysicsLocation(startPosition);
        robotControl.setViewDirection(startDirection);
        robotControl.setGravity(new Vector3f(0, 0, 0));
        
        physicsSpace.add(robotControl);
        rootNode.attachChild(robotNode);
        robotNode.setLocalTranslation(startPosition);
        
        robotNode.addControl(robotControl);
        robotNode.attachChild(robotSpatial);

    }
    
     /**
     * Checks if a crate or robot is near
     * @param crates
     * @param robots
     * @return 
     */
    public boolean checkCollision(List<Crate> crates, List<Robot> robots)
    {
        Vector3f robotPos = getPosition();
        
        for(Crate crate : crates) {
            boolean crateCol = Math.abs(robotPos.x - crate.getPosition().x) < 3.5f && Math.abs(robotPos.z - crate.getPosition().z) < 3.5f;
            if (crateCol) {
                return true;
            }
        }
        
        for(Robot robot : robots) {
            boolean robotCol = Math.abs(robotPos.x - robot.getPosition().x) < 2.5f && Math.abs(robotPos.z - robot.getPosition().z) < 2.5f;
            if (robot != this && robotCol) {
                return true;
            }
        }
        return false;
    }

    /**
     * Moves the robot, should be called every step
     */
    public void move() {
        if (!robotFreeze) {
            Vector3f position = robotSpatial.getLocalTranslation();
            Vector3f newPos = position.add(movement);
            robotSpatial.setLocalTranslation(newPos);
            if (selected) {
                selectArrow.setLocalTranslation(newPos.x, newPos.y + 2, newPos.z);
            }
        }
    }

    /**
     * Updates the movement direction of the robot by a certain angle based on
     * the current direction Postive angle is clockwise
     *
     * @param angle
     */
    public void steer(float angle) {

        // Change angle depending on robot type.
        angle = robotType ? angle : -angle;
        
        
        // Prevent robot to go outside the world bound.
        if (robotSpatial.getWorldTranslation().distance(Vector3f.ZERO) >= 510) {
            robotFreeze = true;
        }

        if (!robotFreeze) {
            // Calculate new angles
            angle *= (float) Math.PI / 180;
            float moveX = (float) (movement.x * Math.cos(angle) - movement.z * Math.sin(angle));
            float moveZ = (float) (movement.x * Math.sin(angle) + movement.z * Math.cos(angle));

            // Implement new angels
            movement = new Vector3f(moveX, movement.y, moveZ);
            movement = movement.normalize();
            movement = movement.mult(speed * 2);
            updateAngle();

            // perform movements, with robot control
//            updateRobotControl(movement);
            
        }
    }
    
    /** 
     * Update the robot controller, not working.
     * @param movement
     */
    @Deprecated
    public void updateRobotControl(Vector3f movement){
        movement = movement.mult(0.02f);
        robotControl.setWalkDirection(movement);
        robotControl.setViewDirection(movement);
        robotControl.setPhysicsLocation(robotSpatial.getWorldTranslation());
        
    }

    /**
     * Updates the robot angle based on the robot direction
     */
    public void updateAngle() {
        Float RobotAngle = new Vector2f(movement.x, movement.z).normalize().angleBetween(new Vector2f(0, 1));
        Quaternion yQuat = new Quaternion();
        yQuat.fromAngleAxis(RobotAngle + (float) Math.PI, new Vector3f(0, 1, 0));
        robotSpatial.setLocalRotation(yQuat);

    }

    /**
     * Update the robot speed based on the total light intensity (max is around
     * 0.6)
     *
     * @param angle
     */
    public void updateSpeed(float angle) {

        // We are only intersted in the strength, not the direction
        angle = Math.abs(angle);

        // Normalize strength
        angle /= 60;

        // Bind the update to only change value slightly.
        angle = Math.min(angle, startSpeed);

        if (robotPlusMotor) {
            speed = startSpeed + angle;
        } else {
            speed = Math.max(startSpeed - angle, startSpeed/3);
        }

        // Bind the speed between 0, and maximum speed
        speed = FastMath.clamp(minSpeed, speed, maxSpeed);

    }

    /**
     * Increase robot size (to highlight selection)
     */
    public void select() {
        rootNode.attachChild(selectArrow);
        Vector3f robotPos = robotSpatial.getLocalTranslation();
        selectArrow.setLocalTranslation(robotPos.x, robotPos.y + 2, robotPos.z);
        selected = true;
    }

    /**
     * Decrease robot size (to higlight selection)
     */
    public void deselect() {
        rootNode.detachChild(selectArrow);
        selected = false;
    }

    /**
     * Scales the robot
     *
     * @param scale
     */
    public void scaleRobot(float scale) {
        robotSpatial.scale(scale);
    }

    /**
     * Deletes the robot
     */
    public void delete() {
        rootNode.detachChild(robotSpatial);
    }

    /**
     * Pauzes the robot
     */
    public void freeze() {
        robotFreeze = true;
    }

    /**
     * Unpauzes the robot
     */
    public void unfreeze() {
        robotFreeze = false;
    }

    public Spatial getSpatial() {
        return robotSpatial;
    }

    public Vector3f getMovement() {
        return movement;
    }

    public float getSpeed() {
        return speed;
    }

    public Vector3f getPosition() {
        return robotSpatial.getLocalTranslation();
    }

    public void setMovement(Vector3f movement) {
        this.movement = movement;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setPosition(Vector3f newPos) {
        robotSpatial.setLocalTranslation(newPos);
    }

    public boolean isSelected() {
        return selected;
    }

    public void reset() {
        this.delete();
        this.create(startPosition, this.startDirection, robotSettings, modelPath);
        this.speed = this.startSpeed;
        this.updateAngle();
        this.freeze();

    }

    public void printStatus() {
        Vector3f location = robotSpatial.getLocalTranslation();

        System.out.println("Robot information");
        System.out.printf("\t%-20s (x=%7.2f, y=%7.2f, z=%7.2f)\n", "Start position", startPosition.x, startPosition.y, startPosition.z);
        System.out.printf("\t%-20s (x=%7.5f, y=%7.5f, z=%7.5f)\n\n", "Start direction", startDirection.x, startDirection.y, startDirection.z);

        System.out.printf("\t%-20s (x=%7.2f, y=%7.2f, z=%7.2f)\n", "Current position", location.x, location.y, location.z);
        System.out.printf("\t%-20s (x=%7.5f, y=%7.5f, z=%7.5f)\n", "Steering direction", movement.x, movement.y, movement.z);
        System.out.printf("\t%-20s %8.2f\n\n", "Speed value", speed);
    }
}
