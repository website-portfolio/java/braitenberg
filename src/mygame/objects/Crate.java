/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.objects;

// Root packages
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;

// Collision things
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;

// Object placement and rotation
import com.jme3.scene.Spatial;
import com.jme3.math.Vector3f;
import com.jme3.math.Quaternion;
import com.jme3.renderer.queue.RenderQueue;

/**
 *
 *
 */
public class Crate {

    // From main for placing objects and getting assets
    private final Node rootNode;
    private final AssetManager assetManager;

    Spatial crate;
    
    // Selection variables
    public boolean selected = false;
    Spatial selectArrow;

    /**
     * Constructor of the robot
     *
     * @param rootNode
     * @param assetManager
     */
    public Crate(Node rootNode, AssetManager assetManager) {
        this.rootNode = rootNode;
        this.assetManager = assetManager;
    }

    /**
     * Creates the crate spatial
     *
     * @param startPos
     */
    public void create(Vector3f startPos)
    {
        Vector3f startPosition = new Vector3f(startPos.x, startPos.y + 1.5f, startPos.z);
        crate = assetManager.loadModel("Models/Box/crate.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat.setTexture("DiffuseMap", assetManager.loadTexture("Models/Box/wood.jpg"));
        mat.setTexture("NormalMap", assetManager.loadTexture("Models/Box/woodnormal.jpg"));
        mat.setBoolean("UseMaterialColors",true);
        mat.setBoolean("VertexLighting", true);
        mat.setColor("Diffuse",ColorRGBA.White);
        mat.setColor("Specular",ColorRGBA.White);
        mat.setFloat("Shininess", 64f); 
        
        crate.setMaterial(mat);
        crate.setLocalTranslation(startPosition);
        crate.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        crate.scale(2);
        rootNode.attachChild(crate);
        
        createSelectArrow();
    }
    
    /**
     * Initialize selection arrow
     */
    public void createSelectArrow()
    {
        selectArrow = assetManager.loadModel("Models/Arrow/Arrow.j3o");
        Quaternion x90Rot = new Quaternion();
        x90Rot.fromAngleAxis((float) -Math.PI / 2, new Vector3f(1, 0, 0));
        selectArrow.setLocalRotation(x90Rot);
        selectArrow.scale(0.02f);
    }

    /**
     * add arrow to highlight selection)
     */
    public void select() {
        rootNode.attachChild(selectArrow);
        Vector3f cratePos = crate.getLocalTranslation();
        selectArrow.setLocalTranslation(cratePos.x, cratePos.y + 4, cratePos.z);
        selected = true;
    }

    /**
     * Remove arrow for selection highlight 
     */
    public void deselect() {
        rootNode.detachChild(selectArrow);
        selected = false;
    }

    /**
     * Scales the crate
     *
     * @param scale
     */
    public void scaleRobot(float scale) {
        crate.scale(scale);
    }

    /**
     * Deletes the crate
     */
    public void delete() {
        rootNode.detachChild(crate);
    }

    public Spatial getSpatial() {
        return crate;
    }

    public Vector3f getPosition() {
        return crate.getLocalTranslation();
    }

    public void setPosition(Vector3f newPos) {
        crate.setLocalTranslation(newPos);
    }

    public boolean isSelected() {
        return selected;
    }
}
