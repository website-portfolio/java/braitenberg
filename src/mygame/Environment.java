package mygame;

// Get control from Main to add stuff to the world
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;

// Objects
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.light.PointLight;

// Math
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.math.Quaternion;

// textures, materials
import com.jme3.shader.VarType;
import com.jme3.texture.Texture;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;

// Imports for creating the physics (floor is a collision object)
import com.jme3.bullet.control.RigidBodyControl;

// For creating a nice surrounding
import com.jme3.texture.Texture.WrapMode;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.util.TangentBinormalGenerator;
import com.jme3.material.RenderState.BlendMode;

/**
 *
 * Environment class
 */
public class Environment {
    
    // Set constants for creations
    private final Node rootNode;
    private final AssetManager assetManager;
    private final PhysicsSpace physicsSpace;
    
    /** Prepare Materials */
    Material wallMat;
    Material floorMat;
    Material floorLayerMat; // Transparent invisible layer for light reflections
    Material skyMat;
    
    /** Prepare geometries and physical nodes for walls and floor. */
    private RigidBodyControl physics_wall;
    private RigidBodyControl physicsFloor;
      
     /** Prepare geometries and physical nodes for bricks */
    private static final Box WALL;
    private static final Box FLOOR;
    
    /** dimensions used for the walls */
    private static final float WALL_LENGTH = 50000f;
    private static final float WALL_HEIGHT = 2f;
    private static final float WALL_WIDTH = 0.5f;
    
    private static final float FLOOR_OFFSET = -0.5f;
            
    static {
        
        /** Initialize the wall geometry */
        WALL = new Box(WALL_LENGTH, WALL_HEIGHT, WALL_WIDTH);
        WALL.scaleTextureCoordinates(new Vector2f(50, 50));

        /** Initialize the floor geometry */
        FLOOR = new Box(WALL_LENGTH + WALL_WIDTH, 0.1f, WALL_LENGTH + WALL_WIDTH);
        FLOOR.scaleTextureCoordinates(new Vector2f(3, 3));
        
    }
   
    
     /**
     * Constructor for Floor class.
     * 
     * @param rootNode The basic node to which the floor gets added on creation.
     * @param assetManager Required for gaining acces to floor textures.
     * @param physicsSpace Required for gaining acces to PhysicsEngine.
     */
    public Environment(Node rootNode, AssetManager assetManager, PhysicsSpace physicsSpace){
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.physicsSpace = physicsSpace;
        
    }
    
    /**
     * Create the environment.
     */
    public void create(){
        this.initMaterials();
        this.initFloor();
        this.initWall();
        
        // TODO Prettify this
        this.initSky();  
    }
    
    /** Initialize the materials used in this scene. */
    public void initMaterials() {
        
        // Initialze wall materials
        wallMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        TextureKey key = new TextureKey("Textures/Terrain/BrickWall/BrickWall.jpg");
        key.setGenerateMips(true);
        Texture tex = assetManager.loadTexture(key);
        wallMat.setTexture("ColorMap", tex);

        
        // Initialze floor materials
        floorMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        
        /** 1) Create terrain material and load four textures into it. */
        floorMat = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");
        floorMat.setTexture("Alpha", assetManager.loadTexture("Textures/Terrain/splat/alphamap.png"));
        
        /** 2) Add GRASS texture into the red layer (Tex1). */
        Texture grass = assetManager.loadTexture("Textures/Terrain/splat/dirt.jpg");
        grass.setWrap(WrapMode.Repeat);
        floorMat.setTexture("Tex1", grass);
        floorMat.setFloat("Tex1Scale", 128f);
        
        /** 3) Add DIRT texture into the green layer (Tex2). */
        Texture dirt = assetManager.loadTexture("Textures/Terrain/splat/grass.jpg");
        dirt.setWrap(WrapMode.Repeat);
        floorMat.setTexture("Tex2", dirt);
        floorMat.setFloat("Tex2Scale", 32f);
        
        /** 4) Add ROAD texture into the blue layer (Tex3). */
        Texture rock = assetManager.loadTexture("Textures/Terrain/splat/road.jpg");
        rock.setWrap(WrapMode.Repeat);
        floorMat.setTexture("Tex3", rock);
        floorMat.setFloat("Tex3Scale", 128f);
        
        /** Create invisible transparent material to reflect light sources. */
        floorLayerMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        floorLayerMat.setParam("Ambient", VarType.Vector4, ColorRGBA.White);
        floorLayerMat.setColor("Diffuse", new ColorRGBA(1, 1, 1, 0.5f));
        floorLayerMat.setBoolean("UseMaterialColors", true);
        floorLayerMat.setColor("Specular", new ColorRGBA(1, 1, 1, 0.5f));
        
        skyMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        TextureKey skyKey = new TextureKey("Textures/Sky/Sky.png");
        skyKey.setGenerateMips(true);
        Texture sky = assetManager.loadTexture(skyKey);
        skyMat.setTexture("ColorMap", sky);
        
    }
    
    /** Make a solid floor and add it to the scene. */
    public void initFloor() {
       
        // Create the floor hitbox geometry
        Geometry geoFloor = new Geometry("Floor", FLOOR);
        geoFloor.setMaterial(floorMat);
        geoFloor.setLocalTranslation(0, FLOOR_OFFSET, 0);
        this.rootNode.attachChild(geoFloor);

        // Create the floor overlay for light reflections
        Box b = new Box(WALL_LENGTH, 1.5f, WALL_LENGTH);
        Geometry device = new Geometry("Device", b);
        device.setMaterial(floorLayerMat);
        floorLayerMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        device.setQueueBucket(Bucket.Transparent);
        rootNode.attachChild(device);

        /* Make the floor physical with mass 0.0f! */
        physicsFloor = new RigidBodyControl(0.0f);
        geoFloor.addControl(physicsFloor);
        physicsSpace.add(physicsFloor);
        TangentBinormalGenerator.generate(geoFloor);
}

   /** 
    * Create the walls around the field (no longer visible on the actual field).
    */
    public void initWall(){
        // Define rotations
        Quaternion rot90 = new Quaternion().fromAngleAxis(FastMath.HALF_PI, Vector3f.UNIT_Y);
        Quaternion rot0 = new Quaternion();
        
        // Create the 4 walls surrounding the playing field.
        this.createWall(new Vector3f(+WALL_LENGTH, WALL_HEIGHT + FLOOR_OFFSET, +WALL_WIDTH), rot90);
        this.createWall(new Vector3f(-WALL_LENGTH, WALL_HEIGHT + FLOOR_OFFSET, -WALL_WIDTH), rot90);
        this.createWall(new Vector3f(-WALL_WIDTH, WALL_HEIGHT + FLOOR_OFFSET, +WALL_LENGTH), rot0);
        this.createWall(new Vector3f(+WALL_WIDTH, WALL_HEIGHT + FLOOR_OFFSET, -WALL_LENGTH), rot0);
    }
        
    /**
     * Create a single wall.
     * 
     * @param translation The starting location of the wall
     * @param rotate The rotation of the wall given as a Quaternion
     */
    public void createWall(Vector3f translation, Quaternion rotate) {
        Geometry geoWall = new Geometry("Wall", WALL);
        geoWall.setMaterial(wallMat);
        geoWall.setLocalTranslation(translation);
        geoWall.rotate(rotate);
        this.rootNode.attachChild(geoWall);
        
        /* Make wall very heavy */
        physics_wall = new RigidBodyControl(200000f);
        geoWall.addControl(physics_wall);
        physicsSpace.add(physics_wall);
    }
    
    /**
     * Create the surrounding sky
     * Not workingable right now
     */
    public void initSky() {
        int radius = 600;
        Sphere sphere = new Sphere(256, 256, radius, false, true);
        Geometry sphereGeo = new Geometry("Shiny rock", sphere);
        
        sphereGeo.setMaterial(skyMat);
        sphereGeo.setLocalTranslation(0, -radius / 2, 0);
        rootNode.attachChild(sphereGeo);    
        
        PointLight sun = new PointLight(Vector3f.ZERO, ColorRGBA.White, radius-5);
        rootNode.addLight(sun);
    }
    
}