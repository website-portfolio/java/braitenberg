/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.lights;

import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author Thymen
 */
abstract public class Light {

    // Required for access to models
    Node rootNode;
    AssetManager assetManager;
    ViewPort viewport;
    
    // Setup spatial
    Spatial light;
    Spatial selectArrow;
    public boolean selected;
    
    
    Light(Node root, AssetManager assetManager, ViewPort viewport){
        this.rootNode = root;
        this.assetManager = assetManager;
        this.viewport = viewport;
    }
    
    abstract public void create();
    
    abstract public void delete();
    
    abstract public void setPosition(Vector3f newPosition);
    
    abstract public void setLocalTranslation(Vector3f addVector);
    
    abstract public void setColor(ColorRGBA color);
    
    abstract public void setRange(float range);
    
    abstract public float distance(Vector3f position);
    
    abstract public float lightIntensity(Vector3f position);
    
    abstract public void select();
    
    abstract public void deselect();
    
    public Spatial getSpatial() {
        return light;
    }
    
    public boolean boxBetween(Vector3f pos, Vector3f dir)
    {
        CollisionResults results = new CollisionResults();
        Ray ray = new Ray(pos, dir);
        rootNode.collideWith(ray, results);

        if (results.size() == 0) {
            return false;
        } else {
            for(CollisionResult collision : results) {
                if(collision.getGeometry().getName().startsWith("crate")) {
                    return true;
                }
            }
            
            return false;
        }
    }
    
    /**
     * Initialize selection arrow
     */
    public void createSelectArrow() {
        selectArrow = assetManager.loadModel("Models/Arrow/Arrow.j3o");
        Quaternion x90Rot = new Quaternion();
        x90Rot.fromAngleAxis((float) -Math.PI / 2, new Vector3f(1, 0, 0));
        selectArrow.setLocalRotation(x90Rot);
        selectArrow.scale(0.02f);
    }
            
}
