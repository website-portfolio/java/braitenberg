/*
 * Basic light template that you can copy and adjust. 
 */
package mygame.lights;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;

/**
 *
 * @author Thymen
 */
public class Base extends Light {

    public Base(Node root, AssetManager assetManager, ViewPort viewport) {
        super(root, assetManager, viewport);
    }
    
    @Override
    public void create() {

    }

    @Override
    public void delete() {

    }
    
    @Override
    public void select() {

    }    
    
    @Override
    public void deselect() {

    }    

    @Override
    public void setPosition(Vector3f newPosition) {

    }
    
    @Override
    public void setLocalTranslation(Vector3f addVector){
        
    }

    @Override
    public void setColor(ColorRGBA color) {

    }

    @Override
    public void setRange(float range) {
    }

    @Override
    public float distance(Vector3f position) {
        return 0f;
    }

    @Override
    public float lightIntensity(Vector3f position) {
        return 0f;
    }

}
