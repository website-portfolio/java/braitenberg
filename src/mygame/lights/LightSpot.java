/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.lights;

// Requireed for model access and placing
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;


import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.light.SpotLight;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.ViewPort;
import com.jme3.shadow.SpotLightShadowFilter;
import com.jme3.shadow.SpotLightShadowRenderer;

/**
 * 
 */
public class LightSpot extends Light {
    
    SpotLight source;
    
    // Defaults
    Vector3f defaultPos = new Vector3f(0f, 0f, 0f);
    Vector3f defaultDir = new Vector3f(0f, -1f, 0f);
    
    float defaultRange = 100f;
    ColorRGBA defaultColor = ColorRGBA.White;
    
    public LightSpot(Node root, AssetManager assetManager, ViewPort viewport) {
        super(root, assetManager, viewport);
    }
        
    @Override
    public void create() {
        // Create Spot light.
        source = new SpotLight();
        source.setSpotRange(defaultRange);
        source.setSpotInnerAngle(3f * FastMath.DEG_TO_RAD);
        source.setSpotOuterAngle(10f * FastMath.DEG_TO_RAD);
        source.setColor(defaultColor);
        source.setPosition(defaultPos);
        source.setDirection(defaultDir);
        rootNode.addLight(source);
        
        createSpotObject(defaultPos, defaultColor, defaultDir);
        createSelectArrow();
        
        setShadow(source, viewport);
        
    }
    
    public void setShadow(SpotLight spot, ViewPort viewport)
    {
        final int SHADOWMAP_SIZE=1024;
//        SpotLightShadowRenderer slsr = new SpotLightShadowRenderer(assetManager, SHADOWMAP_SIZE);
//        slsr.setLight(spot);
//        viewport.addProcessor(slsr);

        SpotLightShadowFilter slsf = new SpotLightShadowFilter(assetManager, SHADOWMAP_SIZE);
        slsf.setLight(spot);
        slsf.setEnabled(true);
        FilterPostProcessor fppSpot = new FilterPostProcessor(assetManager);
        fppSpot.addFilter(slsf);
        viewport.addProcessor(fppSpot);
    }

    @Override
    public void delete() {
        rootNode.detachChild(this.light);
        rootNode.removeLight(this.source);

    }

    @Override
    public void setPosition(Vector3f newPosition) {
        light.setLocalTranslation(newPosition);
        source.setPosition(newPosition);
    }
    
    @Override
    public void setLocalTranslation(Vector3f addVector) {
        light.setLocalTranslation(light.getLocalTranslation().add(addVector));
        source.setPosition(source.getPosition().add(addVector));
    }
    
    @Override
    public void setColor(ColorRGBA color) {
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        light.setMaterial(mat);
        source.setColor(color);
    }

    @Override
    public void setRange(float range) {
        source.setSpotRange(range);
    }

    @Override
    public float distance(Vector3f position) {
//        Vector3f lightVector = new Vector3f(0, 0, 0);
        float l2distance = position.distance(source.getPosition());
        return l2distance;
    }

    @Override
    public float lightIntensity(Vector3f position)
    {
        Vector3f rayDir = source.getPosition().subtract(position);
        if(boxBetween(position, rayDir))
        {
            return 0f;
        }
        
        float beamWidth = (float) (distance(position) * Math.sin(source.getSpotOuterAngle()));
        float lengthMod = 0f;
        float sideMod = 0f;
        float intensity;
        
        if (distance(position) < source.getSpotRange()) {
            lengthMod = ((source.getSpotRange() - distance(position)) / source.getSpotRange());
        }
        
        if (distanceToSpotCentre(position) < beamWidth) {
            sideMod = ((beamWidth - distanceToSpotCentre(position)) / beamWidth);
        }
        
        intensity = lengthMod * sideMod;
        
        return intensity;
    }
    
    public void createSpotObject(Vector3f pos, ColorRGBA color, Vector3f dir) {        
        light = assetManager.loadModel("Models/Light/Flashlight/Flashlight.j3o");
        light.scale(.25f);
        light.setLocalTranslation(pos);
        light.lookAt(light.getLocalTranslation().add(dir), new Vector3f(0, 1, 0));

        rootNode.attachChild(light);
    }
    
    public void spotWidth(float inner, float outer) {
        source.setSpotInnerAngle(inner * FastMath.DEG_TO_RAD);
        source.setSpotOuterAngle(outer * FastMath.DEG_TO_RAD);
    }
    
    public void orientation(Vector3f newDirection) {
        source.setDirection(newDirection);
        light.lookAt(light.getLocalTranslation().add(newDirection), new Vector3f(0, 1, 0));
    }
    
    public float distanceToSpotCentre(Vector3f pos) {
        float x = source.getPosition().x;
        float y = source.getPosition().y;
        float z = source.getPosition().z;

        Vector3f distanceVector = new Vector3f(pos.x - x, pos.y - y, pos.z - z);
        Vector3f directionVector = source.getDirection();
        Vector3f crossVector = distanceVector.cross(directionVector);

        float l2distance = crossVector.length() / directionVector.length();
        return l2distance;
    }
    
    /**
     * add arrow to highlight selection)
     */
    @Override
    public void select() {
        rootNode.attachChild(selectArrow);
        Vector3f lightPos = light.getLocalTranslation();
        selectArrow.setLocalTranslation(lightPos.x, lightPos.y + 2, lightPos.z);
        selected = true;
    }

    /**
     * Remove arrow for selection highlight
     */
    @Override
    public void deselect() {
        rootNode.detachChild(selectArrow);
        selected = false;
    }
        
}
