/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.lights;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.ViewPort;
import com.jme3.shadow.PointLightShadowFilter;

/**
 *
 */
public class LightPoint extends Light {
    
    // Define specific light
    PointLight source;
    
    // Defaults
    Vector3f defaultPos = new Vector3f(0f, 0f, 0f);
    ColorRGBA defaultColor = ColorRGBA.White;
    float defaultRange = 100f;
    
    
    public LightPoint(Node root, AssetManager assetManager, ViewPort viewport) {
        super(root, assetManager, viewport);
    }
    
    @Override
    public void create() {
        source = new PointLight();
        source.setColor(defaultColor);
        source.setPosition(defaultPos);
        source.setRadius(defaultRange);
        rootNode.addLight(source);
        
        createPointObject(defaultPos, defaultColor);
        createSelectArrow();
        
        setShadow(source, viewport);
        
    }
    
    public void setShadow(PointLight point, ViewPort viewport)
    {
        final int SHADOWMAP_SIZE=1024;
//        PointLightShadowRenderer plsr = new PointLightShadowRenderer(assetManager, SHADOWMAP_SIZE);
//        plsr.setLight(point);
//        viewport.addProcessor(plsr);

        PointLightShadowFilter plsf = new PointLightShadowFilter(assetManager, SHADOWMAP_SIZE);
        plsf.setLight(point);
        plsf.setEnabled(true);
        FilterPostProcessor fppPoint = new FilterPostProcessor(assetManager);
        fppPoint.addFilter(plsf);
        viewport.addProcessor(fppPoint);
    }

    @Override
    public void delete() {
        rootNode.detachChild(this.light);
        rootNode.removeLight(this.source);
    }

    @Override
    public void setPosition(Vector3f newPosition) {
        light.setLocalTranslation(newPosition);
        source.setPosition(newPosition);
    }
    
    @Override
    public void setLocalTranslation(Vector3f addVector) {
        light.setLocalTranslation(light.getLocalTranslation().add(addVector));
        source.setPosition(source.getPosition().add(addVector));
    }
    

    @Override
    public void setColor(ColorRGBA color) {
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        light.setMaterial(mat);
        source.setColor(color);
    }

    @Override
    public void setRange(float range) {
        source.setRadius(range);
    }

    @Override
    public float distance(Vector3f position) {
//        Vector3f lightVector = new Vector3f(0, 0, 0);
        float l2distance = position.distance(source.getPosition());
        return l2distance;
    }

    @Override
    public float lightIntensity(Vector3f position) {
        
        Vector3f rayDir = source.getPosition().subtract(position);
        if(boxBetween(position, rayDir)) {
            return 0f;
        }
        
        float intensity = 0f;
        
        if (distance(position) < source.getRadius()) {
            intensity = ((source.getRadius() - distance(position)) / source.getRadius());
        }
        
        return intensity;
    }
    
    public void createPointObject(Vector3f position, ColorRGBA color) {
        light = assetManager.loadModel("Models/Light/Sun/Sun.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        light.setMaterial(mat);
        light.setLocalTranslation(position);
        rootNode.attachChild(light);
    }
    
    /**
     * add arrow to highlight selection)
     */
    @Override
    public void select() {
        rootNode.attachChild(selectArrow);
        Vector3f lightPos = light.getLocalTranslation();
        selectArrow.setLocalTranslation(lightPos.x, lightPos.y + 2, lightPos.z);
        selected = true;
    }

    /**
     * Remove arrow for selection highlight
     */
    @Override
    public void deselect() {
        rootNode.detachChild(selectArrow);
        selected = false;
    }
    
}