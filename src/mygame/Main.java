package mygame;

// Pacakges from my game
import mygame.controller.GuiController;
import mygame.controller.LightControl;
import mygame.controller.RobotControl;

// Basic requirements
import com.jme3.app.SimpleApplication;

// Change image
import com.jme3.system.AppSettings;

// Physics
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.KeyInput;
import com.jme3.math.Ray;

// Gui
import de.lessvoid.nifty.Nifty;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;

// Lights
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;

// Placing objects 
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;

import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import mygame.controller.CrateControl;

/**
 *
 * Main class
 */
public class Main extends SimpleApplication {

    // Gui declarations
    private Nifty nifty;
    GuiController gc = new GuiController(this);

    // Forward declarations
    Environment environment;
    BulletAppState bulletAppState;
    RobotControl robotController;
    LightControl lightController;
    CrateControl crateController;

    // Define time contstraints
    private final int FPS = 5;
    private static double timePassed = 0.;

    // Placing objects
    int placingStates = 3;
    int placing = 0;
    Vector3f placePos = new Vector3f(0, 0, 0);
    Vector3f placeDir = new Vector3f(0, 0, 0);
    Vector2f placePosScreen = new Vector2f(0, 0);
    float placeAreaWidth = 30f;
    float placeAreaHeight = 30f;
    float maxPlaceHeight = 20f;
    String robotSettings;

    Spatial placingObject;
    Spatial mouseObject;

    public static void main(String[] args) {
        Main app = new Main();

        /** Change background image and screen sizes.  */
        AppSettings appSettings = new AppSettings(true);
        appSettings.setSettingsDialogImage("Images/Home.png");
        appSettings.setTitle("Braitenberg vehicles simulator");
        appSettings.setMinWidth(1600);
        appSettings.setMinHeight(900);
        
        app.setSettings(appSettings);
        app.setDisplayStatView(false);

        /**
         * Start application.
         */
        app.start();
    }

    @Override
    public void simpleInitApp() {

        // Set up the camera
        setCamera();

        // Set up Physics engine for the Game.
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
//        bulletAppState.setDebugEnabled(true); // Debug line, for collisions

        // Setup the objects controllers., light has to go before robot, first since robot controller requires it.
        lightController = new LightControl(rootNode, assetManager, viewPort);
        crateController = new CrateControl(rootNode, assetManager);
        robotController = new RobotControl(rootNode, assetManager, getPhysicsSpace(), lightController, crateController);

        // Add light source (upgrade to better source).
        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.5f, -1f, -0.3f).normalizeLocal());
        dl.setColor(ColorRGBA.White);
        rootNode.addLight(dl);

        // Intialize the nifty Gui
        setupGui();

        // Create the game surroundings.
        createEnvironment();
        importScene(0);

        // Initialize object placer.
        initObjectPlacer();
    }
    
    /**
     * Initialize Nifty GUI.
     */
    private void setupGui() {

        NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(
                assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);
        nifty = niftyDisplay.getNifty();
        getGuiViewPort().addProcessor(niftyDisplay);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        // Build screens (gui and scene)
        gc.buildGuiScreen(nifty, gc);
        gc.buildSceneScreen(nifty, gc);
    }

    private void setCamera() {
        /**
         * Adjust camera location and speed.
         */
        cam.setLocation(new Vector3f(-8f, 40f, -60f));
        cam.lookAt(new Vector3f(-8f, -10f, 20f), Vector3f.UNIT_Y);
        flyCam.setMoveSpeed(100);
        flyCam.setZoomSpeed(0);
        flyCam.setDragToRotate(true);
    }

    /**
     * Get the physics spaces from jbullet.
     *
     * @return
     */
    public PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
    }

    /**
     * Control frame updating
     *
     * @param tpf
     */
    @Override
    public void simpleUpdate(float tpf) {

        timePassed += tpf;
        clipCamera();
        

        if (timePassed > 1. / FPS) {
            timePassed = 0.;
        }
        robotController.updateRobots();

        if (placing != 0) {
            updatePlacing();
        }

    }
    
    /** 
     * Prevents the camera to go out of screen.
     */
    public void clipCamera() {
        Vector3f camLocation = cam.getLocation();
        
        // Alternative contraints
        float camLocX = FastMath.clamp(-500, camLocation.x, 500);
        float camLocY = FastMath.clamp(   3, camLocation.y, 500);
        float camLocZ = FastMath.clamp(-500, camLocation.z, 500);
        
        camLocation = new Vector3f(camLocX, camLocY, camLocZ);
        
        // Set a limitperimiter.
        float limit = camLocY < 40 ? 500 : 299;

        // Don't leave the perimeter
        while (camLocation.distance(Vector3f.ZERO) > limit) {
            camLocation = camLocation.mult(0.99f);
        }

        cam.setLocation(camLocation);
        
    }
    

    // Environment
    /**
     * Creating the environment, a floor with a wall surrounding.
     */
    public void createEnvironment() {
        environment = new Environment(rootNode, assetManager, getPhysicsSpace());
        environment.create();
    }

    // Place objects
    /**
     * Create clickListener and create mouseObject
     */
    private void initObjectPlacer() {
        // Activate the mouse button interaction
        inputManager.addMapping("Click", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addListener(actionListener, "Click");
        
        // Activate the delete key
        inputManager.addMapping("Delete", new KeyTrigger(KeyInput.KEY_DELETE));
        inputManager.addListener(actionListener, "Delete");
    }

    /**
     * Listens for mouseclick to start object placement
     */
    private final ActionListener actionListener = new ActionListener() {

        @Override
        public void onAction(String name, boolean keyPressed, float tpf) {
            
            if (name.equals("Delete")) {
                lightController.deleteSelected();
                robotController.deleteSelected();
                crateController.deleteSelected();
            }
            
            if (name.equals("Click") && !keyPressed) {

                if (placing == placingStates) {
                    // Reached final placing state, reset to 0
                    placing = 0;

                    // Enable flycam
                    flyCam.setEnabled(true);
                    flyCam.setDragToRotate(true);

                    // Detach mouse object, and create new object
                    rootNode.detachChild(mouseObject);

                    //place correct object
                    placeSelectedObject(placingStates);

                } else if (placing == 1) {
                    //placing a robot go to 4
                    if (placingStates == 4) {
                        placing = 4;
                    }
                    else if (placingStates == 5)
                    {
                        // finished placing box so end placing sequence
                        placing = 0;

                        // Enable flycam
                        flyCam.setEnabled(true);
                        flyCam.setDragToRotate(true);

                        // Detach mouse object, and create new object
                        rootNode.detachChild(mouseObject);

                        //place correct object
                        placeSelectedObject(placingStates);
                    } 
                    else { 
                        // Reached first placing state, go to 2
                        placing = 2;
                    }

                } else if (placing == 2) {
                    // Reached second placing state, go to 3
                    placing = 3;

                } else if (placing == 0) {
                    //  We are not placing an object, so check for selection.
                    CollisionResults results = new CollisionResults();
                    Vector2f click2d = inputManager.getCursorPosition().clone();
                    Vector3f click3d = cam.getWorldCoordinates(click2d, 0f).clone();
                    Vector3f dir = cam.getWorldCoordinates(click2d, 1f).subtractLocal(click3d).normalizeLocal();
                    Ray ray = new Ray(click3d, dir);
                    rootNode.collideWith(ray, results);

                    if (results.size() == 0) {
                        lightController.deselectAll();
                        robotController.deselectAll();
                        crateController.deselectAll();

                    } else {

                        // Get the name of the target collision
                        String targetName = results.getClosestCollision().getGeometry().getName();
                        Geometry closestSource = results.getClosestCollision().getGeometry();

                        // Check all objects for collisions
                        boolean lightPoint = targetName.startsWith("Flashlight");
                        boolean lightSpot = targetName.startsWith("Sun");
                        boolean robot = targetName.startsWith("Robot");
                        boolean crate = targetName.startsWith("crate");

                        // Perform selection/deselection based on collision type.
                        if (lightPoint || lightSpot) {
                            lightController.SelectCollision(closestSource);
                            robotController.deselectAll();
                            crateController.deselectAll();
                        } else if (robot) {
                            robotController.SelectCollision(closestSource);
                            crateController.deselectAll();
                            lightController.deselectAll();
                        } else if (crate) {
                            crateController.SelectCollision(closestSource);
                            lightController.deselectAll();
                            robotController.deselectAll();
                        } else {
                            System.out.printf("\nSelected unknown mesh: %s", targetName);
                        }
                    }
                }
            }
        }
    };

    /**
     * Create a new object that has to be placed.
     *
     * @param placingStates The differnet types of objects that can be placed.
     */
    public void placeSelectedObject(int placingStates) {
        switch (placingStates) {
            case 2:
                lightController.addPointLight(placePos, ColorRGBA.Green, 5000000f);
                break;

            case 3:
                lightController.addSpotLight(placePos, ColorRGBA.Green, 5000000f, placeDir);
                break;
                
            case 4:
                robotController.addRobot(placePos, placeDir, robotSettings);
                pauseRun();
                break;
                
            case 5:
                crateController.addCrate(placePos);
                break;
        }
    }
    
    /**
     * Creates a new box object with texture and shadow
     * @param pos 
     */
    public void createBoxObject(Vector3f pos)
    {
        Spatial box = assetManager.loadModel("Models/Box/crate.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat.setTexture("DiffuseMap", assetManager.loadTexture("Models/Box/wood.jpg"));
        mat.setTexture("NormalMap", assetManager.loadTexture("Models/Box/woodnormal.jpg"));
        mat.setBoolean("UseMaterialColors",true);
        mat.setBoolean("VertexLighting", true);
        mat.setColor("Diffuse",ColorRGBA.White);
        mat.setColor("Specular",ColorRGBA.White);
        mat.setFloat("Shininess", 64f); 
        box.setMaterial(mat);
        box.setLocalTranslation(pos);
        box.setShadowMode(ShadowMode.CastAndReceive);
        box.scale(2);
        rootNode.attachChild(box);
    }

    /**
     * Control the different translations while placing an object.
     */
    public void updatePlacing() {
        // TODO Abstract from which model is going to be placed
        rootNode.attachChild(mouseObject);
        Vector2f mouseCoords = new Vector2f(inputManager.getCursorPosition());

        float screenWidth = this.settings.getWidth();
        float screenHeight = this.settings.getHeight();

        switch (placing) {
            case 1:
                CollisionResults results = new CollisionResults();
                Vector3f mouseWorldPos = cam.getWorldCoordinates(mouseCoords, 0f).clone();
                Vector3f dir = cam.getWorldCoordinates(mouseCoords, 1f).subtractLocal(mouseWorldPos).normalizeLocal();
                Ray ray = new Ray(mouseWorldPos, dir);
                rootNode.collideWith(ray, results);
                Vector3f mouseFloorPos = new Vector3f(0, 0, 0);
                for (CollisionResult result : results) {
                    if (result.getGeometry().getName().startsWith("Floor")) {
                        mouseFloorPos = result.getContactPoint();
                    }
                }
                mouseObject.setLocalTranslation(mouseFloorPos.x, 1.5f, mouseFloorPos.z);
                placePos.x = mouseFloorPos.x;
                placePos.z = mouseFloorPos.z;
                placePosScreen = mouseCoords;
                break;

            case 2:
                float placeYpos = maxPlaceHeight * (mouseCoords.y / screenHeight);
                mouseObject.setLocalTranslation(placePos.x, placeYpos, placePos.z);
                placePos.y = placeYpos;
                break;

            case 3:
                float placeXDir = 1 - 2 * (mouseCoords.x / screenWidth);
                float placeZDir = 1 - 2 * (mouseCoords.y / screenWidth);
                Quaternion xQuat = new Quaternion();
                xQuat.fromAngleAxis((float) Math.PI * placeXDir, new Vector3f(0, 0, 1));
                Quaternion zQuat = new Quaternion();
                zQuat.fromAngleAxis((float) Math.PI * placeZDir, new Vector3f(1, 0, 0));
                Quaternion endRotation = xQuat.mult(zQuat);
                mouseObject.setLocalRotation(endRotation);

                Quaternion x90Rot = new Quaternion();
                x90Rot.fromAngleAxis((float) -Math.PI / 2, new Vector3f(0, 1, 0));
                Quaternion placeDirRot = endRotation.mult(x90Rot);
                placeDir = placeDirRot.mult(new Vector3f(1, 0, 0));
                break;

            case 4:
                Vector2f mouseDir = new Vector2f(mouseCoords.x - placePosScreen.x, mouseCoords.y - placePosScreen.y);
                Float turnAngle = mouseDir.normalize().angleBetween(new Vector2f(0, -1));
                Quaternion yQuat = new Quaternion();
                yQuat.fromAngleAxis(-turnAngle, new Vector3f(0, 1, 0));
                mouseObject.setLocalRotation(yQuat);
                placeDir = yQuat.mult(new Vector3f(0, 0, -1));
                break;
        }
    }

    // Handle Gui events
    /**
     * Start the run
     */
    public void startRun() {
        System.out.println("Run started");
        robotController.startRun();
    }

    /**
     * Pause the run
     */
    public void pauseRun() {
        System.out.println("Run paused");
        robotController.pauseRun();
    }

    /**
     * Stop the run
     */
    public void stopRun() {
        System.out.println("Run stopped");
        robotController.resetAll();
        robotController.pauseRun();
        setCamera();
    }

    /**
     * Click controller for placing new pointlight
     */
    public void placePointLight() {
        placePos = new Vector3f(0, 0, 0);
        placeDir = new Vector3f(0, 0, 0);
        placingStates = 2;
        placing = 1;
        flyCam.setEnabled(false);
        Spatial sun = assetManager.loadModel("Models/Light/Sun/Sun.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Yellow);
        sun.setMaterial(mat);
        mouseObject = sun.clone();
    }

    /**
     * Click controller for placing new spotlight
     */
    public void placeSpotLight() {
        placePos = new Vector3f(0, 0, 0);
        placeDir = new Vector3f(0, 0, 0);
        placingStates = 3;
        placing = 1;
        flyCam.setEnabled(false);
        Spatial flashLight = assetManager.loadModel("Models/Light/Flashlight/Flashlight.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.White);
        flashLight.setMaterial(mat);
        mouseObject = flashLight.clone();
        mouseObject.scale(.25f);
    }

    /**
     * Click controller for placing new robot
     *
     * @param settings String that contains building information
     */
    public void placeRobot(String settings) {

        placePos = new Vector3f(0, 0, 0);
        placeDir = new Vector3f(0, 0, 0);
        placingStates = 4;
        placing = 1;

        robotSettings = settings;
        String modelPath = robotController.getModelPath(settings);
        mouseObject = assetManager.loadModel(modelPath);
        mouseObject.setLocalRotation(new Quaternion(0, 1, 0, 1));

        flyCam.setEnabled(false);
    }
    
    /**
     * Click controller for placing new box
     */
    public void placeBox() {

        placePos = new Vector3f(0, 0, 0);
        placeDir = new Vector3f(0, 0, 0);
        placingStates = 5;
        placing = 1;

        Spatial box = assetManager.loadModel("Models/Box/crate.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Brown);
        box.setMaterial(mat);
        box.scale(2);
        mouseObject = box.clone();
        mouseObject.setLocalRotation(new Quaternion(0, 1, 0, 1));

        flyCam.setEnabled(false);
    }

    /**
     * Open import scene screen
     */
    public void openSceneScreen() {
        System.out.println("Import scene screen openend");

        // Start scene screen
        nifty.gotoScreen("scene");
    }

    /**
     * Close import scene screen
     */
    public void closeSceneScreen() {
        System.out.println("Import scene screen closed");

        // Start gui screen
        nifty.gotoScreen("gui");
    }

    /**
     * Import scene
     *
     * @param scene
     */
    public void importScene(int scene) {
        System.out.println("Import scene " + scene);

        // Go to gui screen
        nifty.gotoScreen("gui");

        // Clear scene
        clearScene();

        // Set up scene
        switch (scene) {
            case 0:
                // Set up scene 0 (default)
                // Create robots
                robotController.addRobot(new Vector3f(- 4f, 0f, -14f), new Vector3f(   1f, 0f,    2f), "normal slow");
                robotController.addRobot(new Vector3f( 14f, 0f,   2f), new Vector3f(  -3f, 0f, -0.7f), "normal fast");
                robotController.addRobot(new Vector3f(  4f, 0f,  14f), new Vector3f(-1.5f, 0f,   -3f), "crossed slow");
                robotController.addRobot(new Vector3f(-16f, 0f,   0f), new Vector3f(   2f, 0f,  0.3f), "crossed fast");
                
                // Create lights
                lightController.addSpotLight(new Vector3f(0, 50, 0), ColorRGBA.Red, 850000f, new Vector3f(0, -1, 0));
                break;
            
            case 1:
                // Set up scene 1
                // Create robots
                robotController.addRobot( 8f, 0f, 0f, "normal slow");
                robotController.addRobot(-8f, 0f, 0f, "crossed slow");

                // Create lights
                lightController.addSpotLight(new Vector3f(  6, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                lightController.addSpotLight(new Vector3f(-10, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                break;

            case 2:
                // Set up scene 2
                // Create robots
                robotController.addRobot( 8f, 0f, 0f, "normal fast");
                robotController.addRobot(-8f, 0f, 0f, "crossed fast");

                // Create lights
                lightController.addSpotLight(new Vector3f(  6, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                lightController.addSpotLight(new Vector3f(-10, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                break;

            case 3:
                // Set up scene 3
                // Create robots
                robotController.addRobot(  8f, 0f, 0f, "normal slow");
                robotController.addRobot(-16f, 0f, 0f, "crossed slow");
                robotController.addRobot( 16f, 0f, 0f, "normal fast");
                robotController.addRobot( -8f, 0f, 0f, "crossed fast");

                // Create lights
                lightController.addSpotLight(new Vector3f(  6, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                lightController.addSpotLight(new Vector3f(-18, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                lightController.addSpotLight(new Vector3f( 14, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                lightController.addSpotLight(new Vector3f(-10, 18, 7), ColorRGBA.Red, 700000f, new Vector3f(0, -1, 0));
                break;

            case 4:
                // Set up scene 4
                // Empty scene, no setup required
                break;
        }

        /**
         * Initialize robot start positions.
         */
        robotController.pauseRun();
        
    }

    /**
     * Clear the whole scene form robots and lights
     */
    public void clearScene() {
        // Remove robots
        robotController.clearAll();

        // Remove lights
        lightController.clearAll();
        
        
        
    }
}
