package mygame.controller;

// From main
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;
import com.jme3.renderer.ViewPort;

// Setup lists
import java.util.List;
import java.util.ArrayList;

// Lights
import mygame.lights.Light;
import mygame.lights.LightPoint;
import mygame.lights.LightSpot;

// Change objects
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;

/**
 *
 */
public class LightControl {
    
    public List<Light> lights = new ArrayList<>();
    private final Node rootNode;
    private final AssetManager assetManager;
    private final ViewPort viewport;
    
    /**
     * Create the instance
     * @param rootNode Required to add objects to the scene
     * @param assetManager Required to get access to the assets
     * @param viewport Required for shadows
     */
    public LightControl(Node rootNode, AssetManager assetManager, ViewPort viewport) {
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.viewport = viewport;
    }
    
    /**
     * Add a point light
     * @param placePos
     * @param color
     * @param range 
     */
    public void addPointLight(Vector3f placePos, ColorRGBA color, float range) {
        LightPoint pointLight = new LightPoint(rootNode, assetManager, viewport);
        setupLight(pointLight, placePos, color, range);
        lights.add(pointLight);
    }
    
    /**
     * Add a spot light
     * @param placePos
     * @param color
     * @param range
     * @param direction 
     */
    public void addSpotLight(Vector3f placePos, ColorRGBA color, float range, Vector3f direction) {
        LightSpot spotLight = new LightSpot(rootNode, assetManager, viewport);
        setupLight(spotLight, placePos, color, range);
        spotLight.orientation(direction);
        lights.add(spotLight);
    }
    
    /**
     * Setup a light in the game.
     *
     * @param source Light source that has to be setup
     * @param position Position of the light source
     * @param range Strenght of the light source
     * @param color The color of the light source
     */
    public void setupLight(Light source, Vector3f position, ColorRGBA color, float range) {
        source.create();
        source.setPosition(position);
        source.setColor(color);
        source.setRange(range);
    }
    
    /**
     * Check for light collision
     * @param closestSource 
     */
    public void SelectCollision(Geometry closestSource) {
        for (Light light : lights) {
            final boolean collisionObject = light.getSpatial() == closestSource;

            if (collisionObject && !light.selected) {
                light.select();
            }
            else if (collisionObject && light.selected) {
                light.deselect();
            }
            if (!collisionObject && light.selected) {
                light.deselect();
            }

        }
    }
    
    /**
     * deselect all light sources
     */
    public void deselectAll() {
        // Unselect all lights
        for (Light source : lights) {
            if (source.selected) {
                source.deselect();
            }
        }
    }
    
    /**
     * Clear all lights from the controller
     */
    public void clearAll() {
        for (Light light : lights) {
            light.delete();
        }
        lights.clear();
    }
    
    /**
     * Get the lights (this is for the robotController)
     * @return The list of all lights in the scene
     */
    public List<Light> getLights() {
        return lights;
    }
    
    /**
     * Destroy the selected light when delete is pressed
     */
    public void deleteSelected() {
        for (Light light : lights) {
            if (light.selected) {
                light.deselect();
                light.delete();
            }
        }
    }

    
}
