package mygame.controller;

// Nifty GUI
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ImageBuilder;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import mygame.Main;

/**
 *
 * GuiController class
 */
public class GuiController implements ScreenController {

//    Variable declarations
    final private Main mainClass;

    public GuiController(Main main) {
        mainClass = main;
    }

//    Base methods
    @Override
    public void bind(Nifty nifty, Screen screen) {
        System.out.println("bind(" + screen.getScreenId() + ")");
    }

    @Override
    public void onStartScreen() {
        System.out.println("onStartScreen");
    }

    @Override
    public void onEndScreen() {
        System.out.println("onEndScreen");
    }

//    Custom Gui event methods
    public void startButtonClicked() {
        System.out.println("Start button clicked");
        // Call startRun method in Main class
        mainClass.startRun();
    }

    public void pauseButtonClicked() {
        System.out.println("Pause button clicked");
        mainClass.pauseRun();
    }

    public void stopButtonClicked() {
        System.out.println("Stop button clicked");
        mainClass.stopRun();
    }

    public void light1ButtonClicked() {
        mainClass.placePointLight();
    }

    public void light2ButtonClicked() {
        mainClass.placeSpotLight();
    }
    
    public void crateButtonClicked() {
        mainClass.placeBox();
    }

    public void vehicleNormalSlow() {
        mainClass.placeRobot("normal slow");
    }

    public void vehicleNormalFast() {
        mainClass.placeRobot("normal fast");
    }
    
    public void vehicleCrossedSlow() {
        mainClass.placeRobot("crossed slow");
    }

    public void vehicleCrossedFast() {
        mainClass.placeRobot("crossed fast");
    }

    public void importSceneButtonClicked() {
        System.out.println("Import scene button clicked");
        mainClass.openSceneScreen();
    }

    public void closeSceneScreenButtonClicked() {
        System.out.println("Close scene screen button clicked");
        mainClass.closeSceneScreen();
    }

    public void scene1ButtonClicked() {
        System.out.println("Scene 1 button clicked");
        mainClass.importScene(1);
    }

    public void scene2ButtonClicked() {
        System.out.println("Scene 2 button clicked");
        mainClass.importScene(2);
    }

    public void scene3ButtonClicked() {
        System.out.println("Scene 3 button clicked");
        mainClass.importScene(3);
    }

    public void scene4ButtonClicked() {
        System.out.println("Scene 4 button clicked");
        mainClass.importScene(4);
    }

    /**
     * Build gui screen
     *
     * @param nifty
     * @param gc
     */
    public void buildGuiScreen(final Nifty nifty, final GuiController gc) {
        final String iconDim = "48px";
        final String imgDim = "96px";

        nifty.addScreen("gui", new ScreenBuilder("gui") {
            {
                controller(gc);

                layer(new LayerBuilder("foreground") {
                    {
                        childLayoutHorizontal();
                        backgroundColor("#0000");

                        panel(new PanelBuilder("panel_left") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("80%");
                            }
                        });

                        panel(new PanelBuilder("panel_right") {
                            {
                                childLayoutVertical();
                                backgroundColor("#222222f3");
                                height("100%");
                                width("20%");

                                // Title panel
                                panel(new PanelBuilder("title") {
                                    {
                                        childLayoutCenter();
                                        height("9%");
                                        width("100%");

                                        control(new LabelBuilder() {
                                            {
                                                color("#fff");
                                                text("B R A I T E N B E R G   V E H I C L E S");
                                                width("100%");
                                                height("100%");
                                            }
                                        });
                                    }
                                });

                                // Icons panel
                                panel(new PanelBuilder("icons") {
                                    {
                                        childLayoutHorizontal();
                                        backgroundColor("#fff5");
                                        height("10%");
                                        width("100%");
                                        visibleToMouse(true);

                                        // Icons made by Pixel perfect from www.flaticon.com
                                        // Stop button
                                        panel(new PanelBuilder("icons_stop") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("33%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(iconDim);
                                                        width(iconDim);
                                                        filename("Images/Controls/stop.png");
                                                        interactOnClick("stopButtonClicked()");
                                                    }
                                                });
                                            }
                                        });

                                        // Pause button
                                        panel(new PanelBuilder("icons_pause") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("33%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(iconDim);
                                                        width(iconDim);
                                                        filename("Images/Controls/pause.png");
                                                        interactOnClick("pauseButtonClicked()");
                                                    }
                                                });
                                            }
                                        });

                                        // Start button
                                        panel(new PanelBuilder("icons_start") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("33%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(iconDim);
                                                        width(iconDim);
                                                        filename("Images/Controls/play.png");
                                                        interactOnClick("startButtonClicked()");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                                // Vehicles panel
                                panel(new PanelBuilder("vehicles") {
                                    {
                                        childLayoutVertical();
                                        height("30%");
                                        width("100%");

                                        panel(new PanelBuilder("vehicles_text") {
                                            {
                                                childLayoutCenter();
                                                backgroundColor("#111d");
                                                height("20%");
                                                width("100%");

                                                control(new LabelBuilder() {
                                                    {
                                                        color("#fff");
                                                        text("Vehicles");
                                                        width("100%");
                                                        height("100%");
                                                    }
                                                });
                                            }
                                        });

                                        panel(new PanelBuilder("vehicles_select") {
                                            {
                                                childLayoutVertical();
                                                height("80%");
                                                width("100%");
                                                
                                                panel(new PanelBuilder("vehicles_normal") {
                                                    {
                                                        childLayoutHorizontal();
                                                        height("50%");
                                                        width("100%");

                                                        panel(new PanelBuilder("vehicles_normal_fast") {
                                                            {
                                                                childLayoutCenter();
                                                                height("100%");
                                                                width("50%");

                                                                image(new ImageBuilder() {
                                                                    {
                                                                        valignCenter();
                                                                        height(imgDim);
                                                                        width(imgDim);
                                                                        filename("Images/Robots/NormalFast.png");
                                                                        interactOnClick("vehicleNormalFast()");
                                                                    }
                                                                });
                                                            }
                                                        });

                                                        panel(new PanelBuilder("vehicles_normal_slow") {
                                                            {
                                                                childLayoutCenter();
                                                                height("100%");
                                                                width("50%");

                                                                image(new ImageBuilder() {
                                                                    {
                                                                        valignCenter();
                                                                        height(imgDim);
                                                                        width(imgDim);
                                                                        filename("Images/Robots/NormalSlow.png");
                                                                        interactOnClick("vehicleNormalSlow()");
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                                
                                                panel(new PanelBuilder("vehicles_crossed") {
                                                    {
                                                        childLayoutHorizontal();
                                                        height("50%");
                                                        width("100%");
                                                        
                                                        panel(new PanelBuilder("vehicles_crossed_fast") {
                                                            {
                                                                childLayoutCenter();
                                                                height("100%");
                                                                width("50%");

                                                                image(new ImageBuilder() {
                                                                    {
                                                                        valignCenter();
                                                                        height(imgDim);
                                                                        width(imgDim);
                                                                        filename("Images/Robots/CrossedFast.png");
                                                                        interactOnClick("vehicleCrossedFast()");
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        
                                                        panel(new PanelBuilder("vehicles_crossed_slow") {
                                                            {
                                                                childLayoutCenter();
                                                                height("100%");
                                                                width("50%");

                                                                image(new ImageBuilder() {
                                                                    {
                                                                        valignCenter();
                                                                        height(imgDim);
                                                                        width(imgDim);
                                                                        filename("Images/Robots/CrossedSlow.png");
                                                                        interactOnClick("vehicleCrossedSlow()");
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        
                                                    }
                                                });
                                                
                                            }
                                        });
                                    }
                                });
                                
                                // Lights panel
                                panel(new PanelBuilder("lights") {
                                    {
                                        childLayoutVertical();
                                        height("22%");
                                        width("100%");

                                        panel(new PanelBuilder("lights_text") {
                                            {
                                                childLayoutCenter();
                                                backgroundColor("#111d");
                                                height("25%");
                                                width("100%");

                                                control(new LabelBuilder() {
                                                    {
                                                        color("#fff");
                                                        text("Lights");
                                                        width("100%");
                                                        height("100%");
                                                    }
                                                });
                                            }
                                        });

                                        panel(new PanelBuilder("lights_select") {
                                            {
                                                childLayoutHorizontal();
                                                height("75%");
                                                width("100%");

                                                panel(new PanelBuilder("lights_select1") {
                                                    {
                                                        childLayoutCenter();
                                                        height("100%");
                                                        width("50%");

                                                        image(new ImageBuilder() {
                                                            {
                                                                valignCenter();
                                                                height(imgDim);
                                                                width(imgDim);
                                                                filename("Images/Lights/light_circle.png");
                                                                interactOnClick("light1ButtonClicked()");
                                                            }
                                                        });
                                                    }
                                                });

                                                panel(new PanelBuilder("lights_select2") {
                                                    {
                                                        childLayoutCenter();
                                                        height("100%");
                                                        width("50%");

                                                        image(new ImageBuilder() {
                                                            {
                                                                valignCenter();
                                                                height(imgDim);
                                                                width(imgDim);
                                                                filename("Images/Lights/light_line.png");
                                                                interactOnClick("light2ButtonClicked()");
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                                
                                // Obstacles panel
                                panel(new PanelBuilder("obstacles") {
                                    {
                                        childLayoutVertical();
                                        height("22%");
                                        width("100%");

                                        panel(new PanelBuilder("obstacles_text") {
                                            {
                                                childLayoutCenter();
                                                backgroundColor("#111d");
                                                height("25%");
                                                width("100%");

                                                control(new LabelBuilder() {
                                                    {
                                                        color("#fff");
                                                        text("Obstacles");
                                                        width("100%");
                                                        height("100%");
                                                    }
                                                });
                                            }
                                        });

                                        panel(new PanelBuilder("obstacles_select") {
                                            {
                                                childLayoutHorizontal();
                                                height("75%");
                                                width("100%");

                                                panel(new PanelBuilder("obstacles_select1") {
                                                    {
                                                        childLayoutCenter();
                                                        height("100%");
                                                        width("50%");

                                                        image(new ImageBuilder() {
                                                            {
                                                                valignCenter();
                                                                height(imgDim);
                                                                width(imgDim);
                                                                filename("Images/Objects/Crate.png");
                                                                interactOnClick("crateButtonClicked()");
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                                // Import panel
                                panel(new PanelBuilder("import") {
                                    {
                                        childLayoutCenter();
                                        height("6%");
                                        width("100%");

                                        control(new ButtonBuilder("import_scene", "Import scene") {
                                            {
                                                alignCenter();
                                                valignCenter();
                                                width("50%");
                                                height("80%");
                                                interactOnClick("importSceneButtonClicked()");
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }.build(nifty));
    }

    /**
     * Build scene screen
     *
     * @param nifty
     * @param gc
     */
    public void buildSceneScreen(final Nifty nifty, final GuiController gc) {
        final String sceneWidth = "500px";
        final String sceneHeight = "300px";

        nifty.addScreen("scene", new ScreenBuilder("scene") {
            {
                controller(gc);

                layer(new LayerBuilder("foreground2") {
                    {
                        childLayoutHorizontal();
                        backgroundColor("#222222f3");

                        // Scenes panel
                        panel(new PanelBuilder("scenes") {
                            {
                                childLayoutVertical();
                                height("100%");
                                width("100%");

                                panel(new PanelBuilder("scenes_text") {
                                    {
                                        childLayoutHorizontal();
                                        backgroundColor("#111d");
                                        height("10%");
                                        width("100%");

                                        control(new LabelBuilder() {
                                            {
                                                color("#fff");
                                                text("Scenes");
                                                width("85%");
                                                height("100%");
                                            }
                                        });

                                        panel(new PanelBuilder("scenes_close") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("15%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height("30px");
                                                        width("30px");
                                                        filename("Images/Controls/close.png");
                                                        interactOnClick("closeSceneScreenButtonClicked()");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                                panel(new PanelBuilder("scenes_select_a") {
                                    {
                                        childLayoutHorizontal();
                                        height("45%");
                                        width("100%");

                                        panel(new PanelBuilder("scenes_select1") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("50%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(sceneHeight);
                                                        width(sceneWidth);
                                                        filename("Images/Scenes/scene1.png");
                                                        interactOnClick("scene1ButtonClicked()");
                                                    }
                                                });
                                            }
                                        });

                                        panel(new PanelBuilder("scenes_select2") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("50%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(sceneHeight);
                                                        width(sceneWidth);
                                                        filename("Images/Scenes/scene2.png");
                                                        interactOnClick("scene2ButtonClicked()");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                                panel(new PanelBuilder("scenes_select_b") {
                                    {
                                        childLayoutHorizontal();
                                        height("45%");
                                        width("100%");

                                        panel(new PanelBuilder("scenes_select3") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("50%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(sceneHeight);
                                                        width(sceneWidth);
                                                        filename("Images/Scenes/scene3.png");
                                                        interactOnClick("scene3ButtonClicked()");
                                                    }
                                                });
                                            }
                                        });

                                        panel(new PanelBuilder("scenes_select4") {
                                            {
                                                childLayoutCenter();
                                                height("100%");
                                                width("50%");

                                                image(new ImageBuilder() {
                                                    {
                                                        valignCenter();
                                                        height(sceneHeight);
                                                        width(sceneWidth);
                                                        filename("Images/Scenes/scene4.png");
                                                        interactOnClick("scene4ButtonClicked()");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }.build(nifty));
    }

}
