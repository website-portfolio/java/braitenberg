package mygame.controller;

// From main
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;

// Setup lists
import java.util.ArrayList;
import java.util.List;

// Import robots and lights
import mygame.lights.Light;

// Change objects
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import mygame.objects.Robot;

public class RobotControl {

    private final List<Robot> robots = new ArrayList<>();

    private final Node rootNode;
    private final AssetManager assetManager;
    private final PhysicsSpace physicsSpace;
    private final LightControl lightControl;
    private final CrateControl crateControl;

    /**
     * Create the instance
     *
     * @param rootNode Required to add objects to the scene
     * @param assetManager Required to get access to the assets
     * @param physicsSpace Required to access the physics in the scene
     * @param lightControl Required to have interactions with light
     * @param crateControl Required to have interactions with crates
     */
    public RobotControl(Node rootNode, AssetManager assetManager, PhysicsSpace physicsSpace, LightControl lightControl, CrateControl crateControl) {
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.physicsSpace = physicsSpace;
        this.lightControl = lightControl;
        this.crateControl = crateControl;
    }

    /**
     * Converts the input string to a robot model location
     *
     * @param startSettings
     * @return
     */
    public String getModelPath(String startSettings) {
        String[] settings = startSettings.toLowerCase().split(" ");

        String type = settings[0];
        String speed = settings[1];

        type = Character.toUpperCase(type.charAt(0)) + type.substring(1);
        speed = Character.toUpperCase(speed.charAt(0)) + speed.substring(1);

        return "Models/Robot/" + type + "/Robot" + speed + ".j3o";
    }

    /**
     * Add robot
     *
     * @param placePos
     * @param placeDir
     * @param robotSettings
     */
    public void addRobot(Vector3f placePos, Vector3f placeDir, String robotSettings) {
        Robot robot = new Robot(rootNode, assetManager, physicsSpace);
        robot.create(placePos, placeDir, robotSettings, this.getModelPath(robotSettings));
        robots.add(robot);
    }

    /**
     * Create a playable robot in the game.
     *
     * @param xPos
     * @param yPos
     * @param zPos
     * @param robotSettings
     */
    public void addRobot(float xPos, float yPos, float zPos, String robotSettings) {
        this.addRobot(new Vector3f(xPos, yPos, zPos), new Vector3f(0, 0, 0), robotSettings);
    }

    /**
     * Update Robots class.
     */
    public void updateRobots() {
        for (Robot robot : robots) {
            if(!robot.checkCollision(crateControl.getCrates(), robots))  {
                robot.move();
            }
        }

        UpdateRobotSteering();
    }

    /**
     * Updates the steering angle of the robots based upon the light TODO
     * refactor
     */
    public void UpdateRobotSteering() {
        for (Robot robot : robots) {
            if(!robot.checkCollision(crateControl.getCrates(), robots))
            {
                //distance from the center of the robot to the sensor
                float disToSide = 0.5f;
                float disToFront = 1f;

                Vector3f robotDirection = robot.getMovement().normalize();
                Vector3f robotPos = robot.getPosition();

                Vector2f perVecRight = new Vector2f(robotDirection.z, -robotDirection.x);
                Vector2f perVecLeft = new Vector2f(-robotDirection.z, robotDirection.x);

                perVecRight.normalizeLocal();
                perVecLeft.normalizeLocal();
                perVecRight.multLocal(disToSide);
                perVecLeft.multLocal(disToSide);

                Vector3f posRight = robotPos.add(new Vector3f(perVecRight.x, 0f, perVecRight.y));
                Vector3f posLeft = robotPos.add(new Vector3f(perVecLeft.x, 0f, perVecLeft.y));

                posRight = posRight.add(robotDirection.normalizeLocal().multLocal(disToFront));
                posLeft = posLeft.add(robotDirection.normalizeLocal().multLocal(disToFront));

                float intensityRight = 0;
                float intensityLeft = 0;

                int numberOfLights = lightControl.getLights().size();

                for (Light light : lightControl.getLights()) {
                    intensityRight += light.lightIntensity(posRight);
                    intensityLeft += light.lightIntensity(posLeft);
                }

                if (numberOfLights == 0) {
                    intensityRight = 0;
                    intensityLeft = 0;
                } else {
                    intensityRight = intensityRight / numberOfLights;
                    intensityLeft = intensityLeft / numberOfLights;
                }

                if (intensityRight < 0.1 && intensityLeft < 0.1) {
                    robot.steer(0f);
                } else if (intensityRight < 0.1) {
                    robot.steer(-intensityLeft);
                } else if (intensityLeft < 0.1) {
                    robot.steer(intensityRight);
                } else {
                    robot.steer(intensityRight - intensityLeft);
                }

                robot.updateSpeed(intensityRight + intensityLeft);
            }
        }
    }

    /**
     * Select and unselect a robot if there has been clicked upon it.
     *
     * @param closestSource
     */
    public void SelectCollision(Geometry closestSource) {

        for (Robot robot : robots) {
            // check all geometries to find correct robot (this is because car exists out of multiple Geometries)
            boolean collision = false;

            for (int i = 0; i < ((Node) robot.getSpatial()).getChildren().size(); i++) {
                Geometry robotGeom = (Geometry) ((Node) robot.getSpatial()).getChild(i);

                if (robotGeom == closestSource) {
                    collision = true;
                }
            }

            if (collision && !robot.selected) {
                robot.select();
            } else if (robot.selected) {
                robot.deselect();
            }
        }
    }

    /**
     * Deselects all robbots
     */
    public void deselectAll() {
        // Unselect all robots
        for (Robot robot : robots) {
            if (robot.isSelected()) {
                robot.deselect();
            }
        }
    }

    /**
     * Start the run on the scene
     */
    public void startRun() {
        // Do stuff to start the run
        for (Robot robot : robots) {
            robot.unfreeze();
        }
    }

    /**
     * Pause the robbots
     */
    public void pauseRun() {
        // Do stuff to pause the run
        for (Robot robot : robots) {
            robot.freeze();
        }
    }

    /**
     * Remmove all robots from the scene
     */
    public void clearAll() {
        for (Robot robot : robots) {
            robot.delete();
        }
        robots.clear();
    }

    /**
     * Reset all robots, note that we first have to deselect to remove the
     * arrow.
     */
    public void resetAll() {
        deselectAll();
        for (Robot robot : robots) {
            robot.reset();
        }
    }

    /**
     * Get all robots.
     *
     * @return
     */
    public List getRobots() {
        return robots;
    }

    /**
     * Destroy the selected robot when delete is pressed.
     */
    public void deleteSelected() {
        Robot deleteRobot = null;
        for (Robot robot : robots) {
            if (robot.selected) {
                robot.deselect();
                robot.delete();
                deleteRobot = robot;
            }
        }
        if(deleteRobot != null)
        {
            robots.remove(deleteRobot);
        }
    }

}
