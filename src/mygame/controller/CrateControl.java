package mygame.controller;

// From main
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;

// Setup lists
import java.util.List;
import java.util.ArrayList;


// Change objects
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import mygame.objects.Crate;

/**
 *
 */
public class CrateControl {
    
    public List<Crate> crates = new ArrayList<>();
    private final Node rootNode;
    private final AssetManager assetManager;
    
    /**
     * Create the instance
     * @param rootNode Required to add objects to the scene
     * @param assetManager Required to get access to the assets
     */
    public CrateControl(Node rootNode, AssetManager assetManager) {
        this.rootNode = rootNode;
        this.assetManager = assetManager;
    }
    
    /**
     * Add a crate
     * @param placePos
     */
    public void addCrate(Vector3f placePos) {
        Crate crate = new Crate(rootNode, assetManager);
        crate.create(placePos);
        crates.add(crate);
    }
    
    /**
     * Check for light collision
     * @param closestSource 
     */
    public void SelectCollision(Geometry closestSource) {
        for (Crate crate : crates) {
            final boolean collisionObject = crate.getSpatial() == closestSource;
            if (collisionObject && !crate.isSelected()) {
                crate.select();
            }
            else if (collisionObject && crate.isSelected()) {
                crate.deselect();
            }
            if (!collisionObject && crate.isSelected()) {
                crate.deselect();
            }

        }
    }
    
    /**
     * deselect all light sources
     */
    public void deselectAll() {
        // Unselect all lights
        for (Crate crate : crates) {
            if (crate.selected) {
                crate.deselect();
            }
        }
    }
    
    /**
     * Clear all lights from the controller
     */
    public void clearAll() {
        for (Crate crate : crates) {
            crate.delete();
        }
        crates.clear();
    }
    
    /**
     * Get the lights (this is for the robotController)
     * @return The list of all lights in the scene
     */
    public List<Crate> getCrates() {
        return crates;
    }
    
    /**
     * Destroy the selected crate when delete is pressed
     */
    public void deleteSelected() {
        Crate deleteCrate = null; 
        
        for (Crate crate : crates) {
            if (crate.selected) {
                crate.deselect();
                crate.delete();
                deleteCrate = crate;
            }
        }
        
        if(deleteCrate != null)  {
            crates.remove(deleteCrate);
        }
    }

    
}
