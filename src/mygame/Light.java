
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.light.PointLight;
import com.jme3.light.SpotLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Dome;
import com.jme3.scene.shape.Sphere;

/**
 *
 * Light class
 */
public final class Light
{
    // niet gelukkig mee, maar ik zie geen andere oplossing
    SpotLight spot;
    PointLight point;
    Spatial light;
    
    Node rootNode;
    AssetManager assetManager;
    Vector3f defaultDir = new Vector3f(0f,-1f,0f);
    
    
    /**
     * Simple constructor, position, color, range and direction are defaults
     * @param lightKind  Kind of light: 0 = spotlight, 1 = pointlight
     * @param root The rootnode
     * @param assetMan The assetmananger
     */
    public Light(int lightKind, Node root, AssetManager assetMan)
    {
        rootNode = root;
        assetManager = assetMan;
        Vector3f defaultPos = new Vector3f(0f,0f,0f);
        ColorRGBA defaultColor = ColorRGBA.White;
        float defaultRange = 100f;
        
        
        if(lightKind == 0)
        {
            spot = new SpotLight();
            spot.setSpotRange(defaultRange);
            spot.setSpotInnerAngle(3f * FastMath.DEG_TO_RAD);
            spot.setSpotOuterAngle(10f * FastMath.DEG_TO_RAD);
            spot.setColor(defaultColor);
            spot.setPosition(defaultPos);
            spot.setDirection(defaultDir);
            rootNode.addLight(spot);
            createSpotObject(defaultPos, defaultColor, defaultDir);
        }
        else if(lightKind == 1)
        {
            point = new PointLight();
            point.setColor(defaultColor);
            point.setPosition(defaultPos);
            point.setRadius(defaultRange);
            rootNode.addLight(point);
            createPointObject(defaultPos, defaultColor);
        }
    }
    
    /**
     * Extended constructor, spotlight direction still has to be set
     * @param lightKind  Kind of light: 0 = spotlight, 1 = pointlight
     * @param root The rootnode
     * @param assetMan The assetmananger
     * @param pos postion of the light
     * @param color color of the light
     * @param range range of the light
     */
    public Light(int lightKind, Node root, AssetManager assetMan, Vector3f pos, ColorRGBA color, float range)
    {
        rootNode = root;
        assetManager = assetMan;
        
        if(lightKind == 0)
        {
            spot = new SpotLight();
            spot.setSpotRange(range);
            spot.setSpotInnerAngle(3f * FastMath.DEG_TO_RAD);
            spot.setSpotOuterAngle(15f * FastMath.DEG_TO_RAD);
            spot.setColor(color);
            spot.setPosition(pos);
            spot.setDirection(defaultDir);
            rootNode.addLight(spot);
            createSpotObject(pos, color, defaultDir);
        }
        else if(lightKind == 1)
        {
            point = new PointLight();
            point.setColor(color);
            point.setPosition(pos);
            point.setRadius(range);
            rootNode.addLight(point);
            createPointObject(pos, color);
        }
    }
    
    public void createSpotObject(Vector3f pos, ColorRGBA color, Vector3f dir)
    {
//        Dome s = new Dome(2, 32, 0.75f);
//        light = new Geometry("Box", s);
//        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        mat.setColor("Color", color);
        //light.setMaterial(mat);
        light = assetManager.loadModel("Models/Light/Flashlight/Flashlight.j3o");
        light.scale(.25f);
        light.setLocalTranslation(pos);
        light.lookAt(light.getLocalTranslation().add(dir), new Vector3f(0, 1, 0));
        
        rootNode.attachChild(light);
    }
    
    public void createPointObject(Vector3f pos, ColorRGBA color)
    {
        Sphere s = new Sphere(16, 16, 1);
        light = new Geometry("Box", s);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        light.setMaterial(mat);
        light.setLocalTranslation(pos);
        rootNode.attachChild(light);
    }
    
    /**
     * Deletes the light
     */
    public void delete()
    {
        rootNode.detachChild(light);
        if (spot != null)
        {
            rootNode.removeLight(spot);
        }
        else if(point != null)
        {
            rootNode.removeLight(point);
        }
    }
    
    /**
     * Selects the light
     * might by purely a aestatic thing with the clicking happening elsewhere
     */
    public void select()
    {
        light.scale(2f);
    }
    
    /**
     * Deselects the light
     * might by purely an aestatic thing with the clicking happening elsewhere
     */
    public void deselect()
    {
        light.scale(0.5f);
    }
    
    /**
     * Sets the light to the new position
     * @param newPos
     */
    public void setPostion(Vector3f newPos)
    {
        light.setLocalTranslation(newPos);
        if (spot != null)
        {
            spot.setPosition(newPos);
        }
        else if(point != null)
        {
            point.setPosition(newPos);
        }
    }
    
        /**
     * Sets the light to the new position
     * @param addPos
     */
    public void setLocalTranslation(Vector3f addPos)
    {
        light.setLocalTranslation(light.getLocalTranslation().add(addPos));
        if (spot != null)
        {
            spot.setPosition(spot.getPosition().add(addPos));
        }
        else if(point != null)
        {
            point.setPosition(point.getPosition().add(addPos));
        }
    }
    
    /**
     * Changes color of the light
     * @param color
     */
    public void color(ColorRGBA color)
    {
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        light.setMaterial(mat);
        if (spot != null)
        {
            spot.setColor(color);
        }
        else if(point != null)
        {
            point.setColor(color);
        }
    }
    
    /**
     * Changes the range of the light
     * @param range range of the light
     */
    public void range(float range)
    {
        if (spot != null)
        {
            spot.setSpotRange(range);
        }
        else if(point != null)
        {
            point.setRadius(range);
        }
    }
    
    /**
     * Changes the width of the spotlight
     * @param inner inner angle in degrees
     * @param outer outer angle in degrees
     */
    public void spotWidth(float inner, float outer)
    {
        if (spot != null)
        {
            spot.setSpotInnerAngle(inner * FastMath.DEG_TO_RAD);
            spot.setSpotOuterAngle(outer * FastMath.DEG_TO_RAD);
        }
    }
    
    /**
     * Changes the orientation of the spotlight
     * @param newDir
     */
    public void orientation(Vector3f newDir)
    {
        if (spot != null)
        {
            spot.setDirection(newDir);
            light.lookAt(light.getLocalTranslation().add(newDir), new Vector3f(0, 1, 0));
        }
    }
    
    /**
     * Gives the distance of the light to the inputted vector
     * @param pos position of light sensor
     * @return the distance as float
     */
    public float distance(Vector3f pos)
    {
        Vector3f lightVector = new Vector3f(0, 0, 0);
        
        if (spot != null)
        {
            lightVector = spot.getPosition();
        }
        else if (point != null)
        {
            lightVector = point.getPosition();
        }
        
        return lightVector.distance(pos);
    }
    
    /**
     * Gives the distance of the inputted vector to line made by the directionvector of the spotlight
     * @param pos position of light sensor
     * @return the distance as float
     */
    public float distanceToSpotCentre(Vector3f pos)
    {        
        if (spot != null)
        {
            float x = spot.getPosition().x;
            float y = spot.getPosition().y;
            float z = spot.getPosition().z;
            
            Vector3f distanceVector = new Vector3f(pos.x - x, pos.y - y, pos.z - z);
            
            Vector3f directionVector = spot.getDirection();
            
            Vector3f crossVector = distanceVector.cross(directionVector);
            
            float distance = crossVector.length()/directionVector.length();

            return distance;
        }
        else
        {
            System.out.println("Works only with spotlight");
            return -1;
        }
    }
    
    /**
     * Gives the intensity of the light based on the distance to the lightsource
     * @param pos position of light sensor
     * @return the distance as float
     */
    public float lightIntensity(Vector3f pos)
    {
        float intensity = 0;
        
        if (spot != null)
        {
            float lengthMod;
            float sideMod;
            
            if (distance(pos) >= spot.getSpotRange())
            {
                lengthMod = 0f;
            }
            else
            {
                lengthMod = ((spot.getSpotRange() - distance(pos)) / spot.getSpotRange());
            }
            
            float beamWidth = (float) (distance(pos) * Math.sin(spot.getSpotOuterAngle()));
            
            if (distanceToSpotCentre(pos) >= beamWidth)
            {
                sideMod = 0f; 
            }
            else
            {
                sideMod = ((beamWidth - distanceToSpotCentre(pos)) / beamWidth);
            }
            
            intensity = lengthMod * sideMod;
            
//            System.out.println(intensity);
        }
        else if (point != null)
        {
            if (distance(pos) >= point.getRadius())
            {
                intensity = 0;
            }
            else
            {
                intensity = (point.getRadius() - distance(pos) / point.getRadius());
            }
        }
        
        return intensity;
    }
}
