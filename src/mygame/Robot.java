package mygame;

// Get control from Main to add stuff to the world
import com.jme3.scene.Node;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;

// Required for drawing new objects
import com.jme3.math.Vector3f;
import com.jme3.math.Matrix3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Box;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;

// Get Physics engine
//import com.jme3.bullet.objects.VehicleWheel;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.math.FastMath;

// Colission detections
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.Quaternion;
import com.jme3.scene.Spatial;


/**
 *  jmonkeyengine/jme3-examples/src/main/java/jme3test/bullet/TestPhysicsCar.java
 */


/**
 *
 * Robot class
 */
public final class Robot {
    
    // Set constants for creations
    private final Node rootNode;
    private final AssetManager assetManager;
    private final PhysicsSpace physicsSpace;
    
    // Define the robot
    private Node robotNode;
    private Material matBox;
    private Vector3f startLoc;
    public boolean crossed;
    
    // Define vehicle control
    private VehicleControl robot;
    Cylinder wheelMesh;
    
    // Define wheel properties
    Vector3f wheelDirection = new Vector3f(0, -1, 0); // was 0, -1, 0
    Vector3f wheelAxle = new Vector3f(-1, 0, 0); // was -1, 0, 0
    float wheelRadius = 0.5f;
    float wheelRestLength = 0.3f;
    float yOff = 0.3f;
    float xOff = 1f;
    float zOff = 2f;
        
    // Define constants (replace these with values with zero)
    private float steeringValue = 0f;
    private float accelerationValue = -5000f;
    
    float wheelMaxRotation = 0.5f;
    float wheelMaxAcceleration = 1000f;
    
    boolean robotSetup = false;
    boolean robotFreeze = false;
    Vector3f movementDirection = Vector3f.ZERO;
    
    // Define suspension constants.
    private final float stiffness = 120.0f;//200=f1 car
    private final float compValue = 0.2f; //(lower than damp!)
    private final float dampValue = 0.3f;
        
    
    /**
     * Constructor for robot class.
     * 
     * @param rootNode The basic node to which the robot gets added on creation.
     * @param assetManager Required for gaining acces to robot models.
     * @param physicsSpace Required for gaining acces to PhysicsEngine.
     */
    public Robot(Node rootNode, AssetManager assetManager, PhysicsSpace physicsSpace){
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.physicsSpace = physicsSpace;
        
        /** Inital used material */
        this.initMaterial();
    }
    
    /**
     * Initialize material for the chasis and wheels.
     */
    public void initMaterial(){
        
        /** Chasis. */
        matBox = new Material(this.assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matBox.getAdditionalRenderState().setBlendMode(BlendMode.AlphaAdditive);
        matBox.setColor("Color", new ColorRGBA(0, 0, 0, 0));
        
    }
        
    /**
     * Creates a robot and translates it to a specified location.
     * 
     * @param translation Location on which the newly created robot spawns.
     * @param crossed Defines if the wires are crosse or not.
     */
    public void create(Vector3f translation, boolean crossed){
        /** Store start loction */
        startLoc = translation;
        this.crossed = crossed;

        
        /** Create shapes and boxes.  */
        CompoundCollisionShape compoundShape = new CompoundCollisionShape();
        BoxCollisionShape box = new BoxCollisionShape(new Vector3f(1.2f, 0.5f, 2.4f));
        compoundShape.addChildShape(box, new Vector3f(0, 1, 0));
                 
        /** Create robot node and vehicle control. */
        robot = new VehicleControl(compoundShape, 400);
        robotNode = getRobotNode(crossed);
                
        /** Add controls to robot and suspensions */
        robotNode.addControl(robot);
        this.setSuspension(robot);
        
        /** Add frame and wheels */
        robotNode.attachChild(this.createChasis() );
        
        wheelMesh = new Cylinder(16, 16, wheelRadius, wheelRadius * 0.6f, true);
        robotNode.attachChild(this.createWheel(robot, new Vector3f(-xOff, yOff, zOff), "front left"));
        robotNode.attachChild(this.createWheel(robot, new Vector3f( xOff, yOff, zOff), "front right"));
        robotNode.attachChild(this.createWheel(robot, new Vector3f(-xOff, yOff,-zOff), "back left"));
        robotNode.attachChild(this.createWheel(robot, new Vector3f( xOff, yOff,-zOff), "back right"));
                
        /** Add full translation to the node, add to main scene and physics. */
        robot.setPhysicsLocation(translation);
        this.rootNode.attachChild(robotNode);
        physicsSpace.add(robot);
        
    }
    
    /** 
     * Define the layout of the car.
     * @param crossed
     * @return 
     */
    public Node getRobotNode(boolean crossed){
        
        Spatial car;
        Node node = new Node();
        Quaternion rot180 = new Quaternion().fromAngleAxis(FastMath.PI, Vector3f.UNIT_Y);
        
        if (crossed) {
            car = this.assetManager.loadModel("Models/Robot/Crossed/Robot.j3o");
        } else {
            car = this.assetManager.loadModel("Models/Robot/Wired/Robot.j3o");
        }
        
        car.rotate(rot180);
        node.attachChild(car);
        return node;
    }
    
    /**
     * Creates the suspension
     * @param vehicle to which control the suspension gets added.
     */
    public void setSuspension(VehicleControl vehicle){
        vehicle.setSuspensionCompression(compValue * 2.0f * FastMath.sqrt(stiffness));
        vehicle.setSuspensionDamping(dampValue * 2.0f * FastMath.sqrt(stiffness));
        vehicle.setSuspensionStiffness(stiffness);
        vehicle.setMaxSuspensionForce(10000.0f);
    }
    
    /**
     * Create the main frame of the robot.
     * @return 
     */
    private Geometry createChasis(){
        Geometry chasis  = new Geometry("chasis", new Box(xOff-wheelRestLength/2f, yOff, zOff));
        chasis.setMaterial(matBox);
        chasis.setLocalTranslation(startLoc);
        return chasis;
    }

    /** 
     * Create the different wheels.
     * @param vehicle
     * @param name
     * @param translation 
     */
    private Node createWheel(VehicleControl vehicle, Vector3f translation, String name){
        Node node = new Node(name + " node");
        Geometry wheel = new Geometry(name, wheelMesh); 
        
        node.attachChild(wheel);
        wheel.rotate(0, FastMath.HALF_PI, 0);
        wheel.setMaterial(matBox);
        vehicle.addWheel(node, translation, wheelDirection, wheelAxle, wheelRestLength, wheelRadius, name.startsWith("back"));
        return node;
    }
        
    /**
     * Delete the robot from the scene
     */
    public void delete(){
        /** Remove all physics. */
        physicsSpace.removeAll(robotNode);
        
        /** Remove all child nodes and remove it from the rootnode. */
        this.robotNode.detachAllChildren();
        this.rootNode.detachChild(robotNode);
        System.out.println("Succesfully removed robot from parent.");
    }
    
    /**
     * Update call for the robot, this will contain the logic for moving to the lights.
     */
    public void move(){
        
        if (!robotSetup) {
            movementDirection = this.robot.getLinearVelocity();
            robot.accelerate(0);
            robotSetup = true;
        }
        
        this.robot.steer(crossed ? -steeringValue:  steeringValue);
    }   
    
    /**
     * Freezes the robot
     */
    public void freeze() {
        System.out.println("Freezing robot.");
        
        if (!robotFreeze) {
            System.out.println("New movement direction: " + this.robot.getLinearVelocity());
            movementDirection = this.robot.getLinearVelocity();
            robotFreeze = true;
        }
        
        
        this.robot.setLinearVelocity(Vector3f.ZERO);
        this.robot.accelerate(0);
        this.robot.brake(10000f);
        
    }
    
    /** 
     * Unfreezes the robot
     */
    public void unfreeze() {
        System.out.println("Un freezing robot.");//
        this.robot.setLinearVelocity(movementDirection);
        this.robot.brake(0f);
        this.robotSetup = false;
        this.robotFreeze = false;
    }
    
    
    /**
     * Reset the whole robot to its original position
     */
    public void reset(){
        robot.setPhysicsLocation(startLoc);
        robot.setPhysicsRotation(new Matrix3f());
        robot.setAngularVelocity(Vector3f.ZERO);
        robot.resetSuspension();
        
        robot.accelerate(accelerationValue);
        robot.setLinearVelocity(robot.getForwardVector(new Vector3f()));
    }
    
    public void updateSteering(float update) {
        
        boolean upper = (this.steeringValue + update <  wheelMaxRotation);
        boolean lower = (this.steeringValue + update > -wheelMaxRotation);
        
        if (lower && upper) {
            this.steeringValue += update;
        } else if (!lower) {
            this.steeringValue = -wheelMaxRotation;
        } else if (!upper) {
            this.steeringValue =  wheelMaxRotation;
        } else {
            System.out.println("ValueError: hit lower and upper bound.");
            System.exit(1);
        }
        
    }
    
     /**
     * Sets the steering value absolutely
     * @param steer 
     */
    public void setSteering(float steer) {
        
        boolean upper = (steer <  wheelMaxRotation);
        boolean lower = (steer > -wheelMaxRotation);
        
        if (lower && upper) {
            this.steeringValue = steer;
        } else if (!lower) {
            this.steeringValue = -wheelMaxRotation;
        } else if (!upper) {
            this.steeringValue =  wheelMaxRotation;
        } else {
            System.out.println("ValueError: hit lower and upper bound.");
            System.exit(1);
        }
    }
    
    public void updateAcceleration(float update) {
        
        boolean upper = (this.accelerationValue + update < 0);
        boolean lower = (this.accelerationValue + update > -wheelMaxAcceleration);  // We are driving backwards :S
        
        if (lower && upper) {
            this.accelerationValue += update;
        } else if (!lower) {
            this.accelerationValue = -wheelMaxAcceleration;
        } else if (!upper) {
            this.accelerationValue = 0;
        } else {
            System.out.println("ValueError: hit lower and upper bound.");
            System.exit(1);
        }
        
    }
    
        /**
     * Returns the forward facing direction of the robot. Should not return 
     * the movement direction, but I am not sure if it does
     * @return the direction of the robot
     */
    public Vector3f getDirection()
    {
        return robot.getForwardVector(Vector3f.ZERO);
    }
    
    /**
     * Returns the position of the center of the robot
     * @return the position of the robot
     */
    public Vector3f getPosition()
    {
        return robot.getPhysicsLocation();
    }
    
    public void printStatus() {
        Vector3f location = robot.getPhysicsLocation();
                
        System.out.println("Robot information"); 
        System.out.printf("\t%-20s (x=%5.2f, y=%5.2f, z=%5.2f)\n", "Start position", startLoc.x, startLoc.y, startLoc.z);
        System.out.printf("\t%-20s (x=%5.2f, y=%5.2f, z=%5.2f)\n", "Current position", location.x, location.y, location.z);
        System.out.printf("\t%-20s %8.2f\n", "Steering value", this.steeringValue);
        System.out.printf("\t%-20s %8.2f\n\n", "Acceleration value", this.accelerationValue);
        
    }
    
    public void setLocalTranslation(float x, float y, float z) {
        setLocalTranslation(new Vector3f(x, y, z));
    }
    
    public void setLocalTranslation(Vector3f translation) {
        
        Vector3f currentLocation = robot.getPhysicsLocation();
        
        
        this.delete();
        this.create(currentLocation.add(translation), crossed);
        this.reset();
        
    }
    
}